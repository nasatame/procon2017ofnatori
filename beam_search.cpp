#include "beam_search.h"

//getNextStateOnly
//一個だけはまるときに列挙する。構想だけで消えていった。

//isSmall追加
//std::vector<Puzzle::state> Puzzle::state::getNextStateNotSmall()
//小さいやつを最初のほうは組み付け対象から除外する。彼も消えていった。

void Puzzle::state::getNextState(std::vector<state>& states, std::unordered_map<unsigned long long int, std::vector<unsigned>>& hash)
{

	//同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {
		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/
					if (equalDistance(piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
						piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1],
						main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {
						Puzzle::frame t;
						if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
							state temporaryState(t, p);
							if (p.size() == 0) {
								states.push_back(temporaryState);
							}
							unsigned long long int temporaryStateHashKey = temporaryState.getHash();
							bool f = true;

							for (int j = 0; j < hash[temporaryStateHashKey].size(); j++) {
								unsigned likeHashNum = hash[temporaryStateHashKey][j];
								if (states[likeHashNum] == temporaryState) {
									f = false;
									break;
								}
							}
							if (f) {
								hash[temporaryStateHashKey].push_back(states.size());
								states.push_back(temporaryState);
							}
						}
					}
				}
			}
		}


	}

	//反転後同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {
		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/
					if (equalDistance(piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
						piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1],
						main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {
						Puzzle::frame t;
						if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
							state temporaryState(t, p);
							if (p.size() == 0) {
								states.push_back(temporaryState);
							}
							unsigned long long int temporaryStateHashKey = temporaryState.getHash();
							bool f = true;

							for (int j = 0; j < hash[temporaryStateHashKey].size(); j++) {
								unsigned likeHashNum = hash[temporaryStateHashKey][j];
								if (states[likeHashNum] == temporaryState) {
									f = false;
									break;
								}
							}
							if (f) {
								hash[temporaryStateHashKey].push_back(states.size());
								states.push_back(temporaryState);
							}
						}
					}
				}
			}
		}

		pieces[s].ope.reverseFlag = false;

	}

	//内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000001)
					{
						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							state temporaryState(t, p);
							if (p.size() == 0) {
								states.push_back(temporaryState);
							}
							unsigned long long int temporaryStateHashKey = temporaryState.getHash();
							bool f = true;

							for (int j = 0; j < hash[temporaryStateHashKey].size(); j++) {
								unsigned likeHashNum = hash[temporaryStateHashKey][j];
								if (states[likeHashNum] == temporaryState) {
									f = false;
									break;
								}
							}
							if (f) {
								hash[temporaryStateHashKey].push_back(states.size());
								states.push_back(temporaryState);
							}
						}
					}
				}
			}
		}


	}

	//反転後内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {
		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;


		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000001)
					{
						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							state temporaryState(t, p);
							if (p.size() == 0) {
								states.push_back(temporaryState);
							}
							unsigned long long int temporaryStateHashKey = temporaryState.getHash();
							bool f = true;

							for (int j = 0; j < hash[temporaryStateHashKey].size(); j++) {
								unsigned likeHashNum = hash[temporaryStateHashKey][j];
								if (states[likeHashNum] == temporaryState) {
									f = false;
									break;
								}
							}
							if (f) {
								hash[temporaryStateHashKey].push_back(states.size());
								states.push_back(temporaryState);
							}
						}
					}
				}
			}
		}

		pieces[s].ope.reverseFlag = false;

	}
}

int Puzzle::state::evaluateKinnNiku()
{
	//式： ( 組み合わせ前　+　図形 - 組み合わせ後 ) / 2

	//この式で新しく組み付けたピースのくっついている周囲長が求まるけど。実装途中であきらめた。

	//実装途中
	//組み合わせ後の枠の長さを求める。
	double length = -main_frame.perimeter();
	//ピースの長さを求める。

	//length += main_frame.pre_perimeter();

	return length;
}

//hash関数を使って同一検索の高速化をしている。
//同一検索は、同じやつを列挙しないようにするために必要。

//main_frame.inners().size() >= 1
unsigned long long int Puzzle::state::getHash() const {
	//hash範囲 0〜12345678901350とする。
	//エラー値 12345678999999
	//piece_hashとframe_hashをまとめて12345678901357(素数)で割ったもの。
	unsigned long long int total_hash = 12345678999999;
	long long int piece_hash = 0;
	long long int frame_hash = 1;
	/*
		long long int piece_hash_stocker = 0;
		for (auto p : pieces) {
			piece_hash += piece_hash_stocker * p.piece_num + 7;
			piece_hash_stocker = 0;
		}*/

	for (unsigned int i = 0; i < main_frame.inners().size(); i++) {
		for (unsigned int j = 0; j < main_frame.inners()[i].size() - 1; j++) {//ここの-1には悩まされた。
			frame_hash *= ((main_frame.inners()[i][j].x()) + (main_frame.inners()[i][j].y()) + 7);
			frame_hash %= 12345678901357;
		}
	}

	total_hash = frame_hash;

	return total_hash;
}

//解の可能性があるか。
bool Puzzle::state::isAnswerPossibility()
{

	double minimize = 1000 * 1000;

	for (auto & i : main_frame.inners()) {
		minimize = std::min(minimize, -boost::geometry::area(i));
	}

	if (main_frame.inners().size() == 0) return true;

	for (auto & i : pieces) {
		if (minimize + 0.01 >= boost::geometry::area(Puzzle::piece_manager::get_instance().getPolygon(i.piece_num))) {
			return true;
		}
	}

	return false;
}

int Puzzle::state::minPieceArea()
{
	double minimize = 1000 * 1000;
	for (auto & i : pieces) {
		if (minimize + 0.01 >= boost::geometry::area(Puzzle::piece_manager::get_instance().getPolygon(i.piece_num))) {
			minimize = boost::geometry::area(Puzzle::piece_manager::get_instance().getPolygon(i.piece_num));
		}
	}
	return minimize;
}


int Puzzle::state::minInnerArea() {
	double minimize = 1000 * 1000;

	for (auto & i : main_frame.inners()) {
		minimize = std::min(minimize, -boost::geometry::area(i));
	}

	return minimize;
}

//ピースとフレームで同じ辺の長さがあるか？結局微妙だった
bool Puzzle::state::equalEdge()
{

	if (main_frame.inners().size() == 0)
		return true;

	double minimize = 1000*1000;

	for (int i = 0; i < main_frame.inners().size(); i++) {
		for(int j = 0; j < main_frame.inners()[i].size()-1; j++)
			minimize = std::min(minimize, boost::geometry::distance(main_frame.inners()[i][j],main_frame.inners()[i][j+1]) );
	}

	for (auto& i : pieces) {
		for (int j = 0; j < piece_manager::get_instance().getPolygon(i.piece_num).outer().size() - 1; j++) {
			if (minimize + 0.01 >=
				boost::geometry::distance(piece_manager::get_instance().getPolygon(i.piece_num).outer()[j]
					, piece_manager::get_instance().getPolygon(i.piece_num).outer()[j + 1])) {
				return true;
			}
		}
	}

	return false;
}

//これもequalEdgeと同じ、微妙
bool Puzzle::state::equalDegree()
{

	for (int i = 0; i < main_frame.inners().size(); i++) {
		for (int j = 0; j < main_frame.inners()[i].size() - 1; j++) {

			if (M_PI / 2 > getExternalAngle(main_frame, i, j) + 0.01) {
				bool flag = false;
				for (unsigned s = 0; s < pieces.size(); s++) {
					for (unsigned k = 0; k < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; k++) {
						if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), k) - getExternalAngle(main_frame, i, j)) < 0.01) {
							flag = true;
						}
					}
				}
				if (!flag) {
					return false;
				}
			}

		}
	}

	return true;
}

double Puzzle::state::minPieceEdge()
{

	double minimize = 1000 * 1000;

	for (auto& i : pieces) {
		for (int j = 0; j < piece_manager::get_instance().getPolygon(i.piece_num).outer().size() - 1; j++) {
			minimize = std::min(minimize,
				boost::geometry::distance(piece_manager::get_instance().getPolygon(i.piece_num).outer()[j]
					, piece_manager::get_instance().getPolygon(i.piece_num).outer()[j + 1])
			);
		}
	}

	return minimize;

}

double Puzzle::state::minInnerEdge()
{

	double minimize = 1000 * 1000;

	for (int i = 0; i < main_frame.inners().size(); i++) {
		for (int j = 0; j < main_frame.inners()[i].size() - 1; j++)
			minimize = std::min(minimize, boost::geometry::distance(main_frame.inners()[i][j], main_frame.inners()[i][j + 1]));
	}

	return minimize;
}

double Puzzle::state::minPieceAngle()
{
	double minimize = 10;

	for (unsigned s = 0; s < pieces.size(); s++) {
		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
			minimize = std::min(
				minimize,
				Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j)
			);

		}
	}

	return minimize / M_PI * 180;

}

double Puzzle::state::minInnerAngle()
{
	double minimize = 10;

	for (int i = 0; i < main_frame.inners().size(); i++) {
		for (int j = 0; j < main_frame.inners()[i].size() - 1; j++) {

			minimize = std::min(
				getExternalAngle(main_frame, i, j),
				minimize
			);
		}
	}

	return minimize/M_PI * 180;
}

bool Puzzle::operator<(const state & a, const state & b)
{
	return a.score < b.score;
}

bool Puzzle::operator==(const state & a, const state & b)
{
	if (boost::geometry::equals(a.main_frame.polygon, b.main_frame.polygon)) {

		for (unsigned i = 0; i < a.pieces.size(); i++) {
			if (a.pieces[i].piece_num != b.pieces[i].piece_num) {
				return false;
			}
		}

		return true;
	}

	return false;
}

//全探索のコード、beam_searchとは？全ての原点
Puzzle::state Puzzle::beam_search::solve(Puzzle::frame f, std::vector<Puzzle::piece_info> pieces)
{

	std::vector<Puzzle::state> nowState(0);
	std::vector<Puzzle::state> nextState(0);

	int count = 0;
	int answer_count = 0;

	nowState.push_back(state(f, pieces));

	while (true) {

		std::unordered_map<unsigned long long int, std::vector<unsigned>> hash;

		for (int i = 0; i < nowState.size(); i++) {
			auto & temporaryState = nowState[i];

			temporaryState.getNextState(nextState, hash);

		}

		if (nextState[0].main_frame.inners().size() == 0) {
			return nextState[0];
		}

		std::swap(nowState, nextState);

		nextState.clear();

	}

}

//主力、GUIでこまめに制御しながら解を探す。
//実質ビームサーチ。というかまんまビームサーチ
int Puzzle::beam_search::solveGuiSuzukiFunc(std::vector<Puzzle::state>& nowState, std::vector<Puzzle::state>& nextState, int funcEnum, int funcEval, Puzzle::state & answer, std::mutex & mtx, int & progressShare)
{
	int answer_count = 0;

	int progress = 0;

	std::unordered_map<unsigned long long int, std::vector<unsigned>> hash;

	for (int i = 0; i < nowState.size(); i++) {
		auto & temporaryState = nowState[i];

		if ((100 * i) / nowState.size() != progress) {
			progress = (100 * i) / nowState.size();
			mtx.lock();
			if (progressShare == -1) {
				mtx.unlock();
				return 0;
			}
			else {
				progressShare = progress;
				mtx.unlock();
			}
		}

		//配置情報により組付けられるやつを組み付ける。評価関数前に実装
		//配置情報にあるのをスキップする。if文で解決。
		//配置情報と重なっているのを無視する。

		std::vector<state> temporaryNextState;

		switch (funcEnum)
		{
		case 0://all
			temporaryNextState = temporaryState.getNextState();
			break;
		case 1://deg
			temporaryNextState = temporaryState.getNextStateDeg();
			break;
		case 2://deg plus
			temporaryNextState = temporaryState.getNextStateDegPlus();
			break;
		case 3://side
			temporaryNextState = temporaryState.getNextStateSide();
			break;
		case 4://side plus
			temporaryNextState = temporaryState.getNextStateSidePlus();
			break;
		case 5://side deg plus
			temporaryNextState = temporaryState.getNextStateEval();
			break;
		default://all
			temporaryNextState = temporaryState.getNextState();
			break;
		}

		for (auto & temporaryState2 : temporaryNextState) {
			if (temporaryState2.main_frame.inners().size() == 0) {
				answer = temporaryState2;
				answer.score = 100;
				answer_count++;
			}

			bool f = true;

			unsigned long long int temporaryStateHashKey = temporaryState2.getHash();

			for (int j = 0; j < hash[temporaryStateHashKey].size(); j++) {
				unsigned likeHashNum = hash[temporaryStateHashKey][j];
				//列挙済みなら除外
				if (nextState[likeHashNum] == temporaryState2) {
					f = false;
					//nextState[likeHashNum].score += 1;//同じやつがたくさん出現したときに評価値を上げる工夫うざいだけだった
					break;
				}
			}


			if (f) {


				//答えになる可能性がないならスキップする
				if (!temporaryState2.isAnswerPossibility()) {
					continue;
				}

				//組み付けるかつ答えになりえないならcontinue;

				bool isAnswer = true;

				while (true) {

					bool f = true;
					for (int arrange_num = 0; arrange_num < temporaryState2.pieces.size(); arrange_num++) {

						if (temporaryState2.pieces[arrange_num].piece_arrange == false) {
							continue;
						}

						Puzzle::frame result;
						//ここですでに重なることは考慮しない
						const int arrange_result = Puzzle::unionByArrange(temporaryState2.main_frame, piece_manager::get_instance().getData(temporaryState2.pieces[arrange_num].piece_num).rotation[0], result);

						if ( arrange_result == 0) {
							temporaryState2.main_frame = result;
							temporaryState2.pieces.erase(temporaryState2.pieces.begin() + arrange_num);
							arrange_num--;
							f = false;
						}
						else if (arrange_result == 2) {
							isAnswer = false;
						}
					}
					if (f)
						break;
				}

				if (!isAnswer) {
					continue;
				}

				temporaryState2.score = 0;
				//評価値の設計。
				//temporaryState2.score = boost::geometry::area(temporaryState2.main_frame.polygon);
				//****前の状態の評価を引き継ぐかは考えどころ****
				//とりあえず、自分で決めて

				switch (funcEval)
				{
				case 0://Edge
					temporaryState2.score = (temporaryState.main_frame.perimeter()
						+ temporaryState2.main_frame.pre_perimeter()
						- temporaryState2.main_frame.perimeter()) / 2;
					break;
				case 1://EdgePlus
					temporaryState2.score = (temporaryState.main_frame.perimeter()
						+ temporaryState2.main_frame.pre_perimeter()
						- temporaryState2.main_frame.perimeter()) / 2 + temporaryState.score / 10;
					break;
				case 2://EdgePercentPlus
					temporaryState2.score = (temporaryState.main_frame.perimeter()
						+ temporaryState2.main_frame.pre_perimeter()
						- temporaryState2.main_frame.perimeter()) * 100 / (2 * temporaryState2.main_frame.pre_perimeter()) + temporaryState.score / 10;
					break;
				case 3://EdgePlus&Hole
					temporaryState2.score = (temporaryState.main_frame.perimeter()
						+ temporaryState2.main_frame.pre_perimeter()
						- temporaryState2.main_frame.perimeter()) / 2 + temporaryState.score / 10 - 10 * temporaryState2.main_frame.inners().size();
					break;
				case 4://None
					temporaryState2.score = 0;
					break;
				case 5://超強力な除外機能を持たせる。
					if (!temporaryState2.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
						continue;
					}
					if (!temporaryState2.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
						continue;
					}

					temporaryState2.score = (temporaryState.main_frame.perimeter()
						+ temporaryState2.main_frame.pre_perimeter()
						- temporaryState2.main_frame.perimeter()) / 2 + temporaryState.score / 2;

					break;
				case 6://ByKitajima
					if (true) {
						if (!temporaryState2.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
							continue;
						}
						if (!temporaryState2.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
							continue;
						}

						int holeNum = temporaryState2.main_frame.inners().size();

						int edgePercent = (temporaryState.main_frame.perimeter()
							+ temporaryState2.main_frame.pre_perimeter()
							- temporaryState2.main_frame.perimeter()) * 100 / (2 * temporaryState2.main_frame.pre_perimeter());

						temporaryState2.score = edgePercent*edgePercent - holeNum * 5;

					}
					break;
				case 7://ByKitajima2 getNewPieceEdgePercent回りでのデバッグをしないと使用禁止
					if (true) {
						if (!temporaryState2.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
							continue;
						}
						if (!temporaryState2.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
							continue;
						}

						int holeNum = temporaryState2.main_frame.inners().size();

						int edgePercent = temporaryState2.main_frame.getNewPieceEdgePercent(temporaryState.main_frame);

						temporaryState2.score = edgePercent - holeNum * 10;

					}
					break;
				default:
					temporaryState2.score = (temporaryState.main_frame.perimeter()
						+ temporaryState2.main_frame.pre_perimeter()
						- temporaryState2.main_frame.perimeter()) / 2;
					break;
				}



				//完全にぴったりはまってたら評価値大アップ
				if (((temporaryState.main_frame.perimeter() + temporaryState2.main_frame.pre_perimeter() - temporaryState2.main_frame.perimeter()) / 2) / temporaryState2.main_frame.pre_perimeter() > 0.99) {
					temporaryState2.score += 100000;
				}

				//重複削除のためにmapにぶち込む
				hash[temporaryStateHashKey].push_back(nextState.size());
				nextState.push_back(temporaryState2);

			}
		}
	}


	//ここでビーム幅をつけて枝狩り。
	sort(nextState.begin(), nextState.end(), [](const state& a, const state& b) {
		return a.score > b.score;
	});

	if (nextState.size() > beam_width)
		nextState.erase(nextState.begin() + beam_width, nextState.end());

	return answer_count;
}

//期待のエース。時間で区切らないchokudaiサーチ。なぜ時間で区切らないかというと、GUIのほうでこちらに中止を指示できるから。時間で区切るメリットがない。
//ビームサーチと大体一緒
int Puzzle::beam_search::chokudaiSearchBySuzuki(std::vector<Puzzle::state>& nowState, std::vector<Puzzle::state>& nextState, int funcEnum, int funcEval, Puzzle::state& answer, std::mutex& mtx, int & progressShare)
{

	const int MaxTurn = 50;
	std::unordered_map<unsigned long long int, std::vector<state>> hash;

	std::priority_queue<state> HStates[51];

	for (int i = 0; i < nowState.size(); i++) {
		HStates[0].push(nowState[i]);
	}

	int ChokudaiWidth = 1;

	//秒数でやるのが一般的だがあえてここではこうしている。
	//結果によっては切り替える。
	const int WIDTH = 10;

	int redo = 0;
	while (WIDTH > ++redo) {

		mtx.lock();
		if (progressShare == -1) {
			mtx.unlock();
			break;
		}
		else {
			progressShare = 100.0 * redo / WIDTH;
			mtx.unlock();
		}



		for (size_t t = 0; t < MaxTurn; t++) {
			//Puzzle::file_manager::output_error(std::to_string(t) + ":" + std::to_string(HStates[t].size()));
			for (size_t i = 0; i < ChokudaiWidth; i++) {
				if (HStates[t].size() <= 0) break;
				auto NowState = HStates[t].top();
				nextState.push_back(NowState);
				HStates[t].pop();

				//funcEnumによって関数を切り替える。
				std::vector<Puzzle::state> NextStates;

				switch (funcEnum)
				{
				case 0://all
					NextStates = NowState.getNextState();
					break;
				case 1://deg
					NextStates = NowState.getNextStateDeg();
					break;
				case 2://deg plus
					NextStates = NowState.getNextStateDegPlus();
					break;
				case 3://side
					NextStates = NowState.getNextStateSide();
					break;
				case 4://side plus
					NextStates = NowState.getNextStateSidePlus();
					break;
				case 5://side deg plus
					NextStates = NowState.getNextStateEval();
					break;
				default://all
					NextStates = NowState.getNextState();
					break;
				}

				for (auto& NextState : NextStates) {

					bool flag = true;

					unsigned long long int temporaryStateHashKey = NextState.getHash();

					for (int j = 0; j < hash[temporaryStateHashKey].size(); j++) {
						if (hash[temporaryStateHashKey][j] == NextState) {
							flag = false;
							break;
						}
					}


					if (flag) {


						//答えになる可能性がないならスキップする
						if (!NextState.isAnswerPossibility()) {
							continue;
						}


						//組み付けるかつ答えになりえないならcontinue;

						bool isAnswer = true;

						while (true) {

							bool f = true;
							for (int arrange_num = 0; arrange_num < NextState.pieces.size(); arrange_num++) {

								if (NextState.pieces[arrange_num].piece_arrange == false) {
									continue;
								}

								Puzzle::frame result;
								//ここですでに重なることは考慮しない
								const int arrange_result = Puzzle::unionByArrange(NextState.main_frame, piece_manager::get_instance().getData(NextState.pieces[arrange_num].piece_num).rotation[0], result);

								if (arrange_result == 0) {
									NextState.main_frame = result;
									NextState.pieces.erase(NextState.pieces.begin() + arrange_num);
									arrange_num--;
									f = false;
								}
								else if (arrange_result == 2) {
									isAnswer = false;
								}
							}
							if (f)
								break;
						}

						if (!isAnswer) {
							continue;
						}


						hash[temporaryStateHashKey].push_back(NextState);

						//funcEvalによって関数を切り替える。

						switch (funcEval)
						{
						case 0://Edge
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2;
							break;
						case 1://EdgePlus
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2;
							break;
						case 2://EdgePercentPlus
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) * 100 / (2 * NextState.main_frame.pre_perimeter()) + NowState.score / 10;
							break;
						case 3://EdgePlus&Hole
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2
								- NextState.main_frame.inners().size() * 100;
							break;
						case 4://None
							NextState.score = 0;
							break;
						case 5://超強力な除外機能を持たせる。
							if (!NextState.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
								continue;
							}
							if (!NextState.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
								continue;
							}

							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2;

							break;
						case 6://ByKitajima
							if (true) {
								if (!NextState.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
									continue;
								}
								if (!NextState.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
									continue;
								}

								int holeNum = NextState.main_frame.inners().size();

								int edgePercent = (NowState.main_frame.perimeter()
									+ NextState.main_frame.pre_perimeter()
									- NextState.main_frame.perimeter()) * 100 / (2 * NextState.main_frame.pre_perimeter());

								NextState.score = edgePercent*edgePercent - holeNum * 5;

							}
							break;
						case 7://ByKitajima2 getNewPieceEdgePercent回りでのデバッグをしないと使用禁止
							if (true) {
								if (!NextState.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
									continue;
								}
								if (!NextState.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
									continue;
								}

								int holeNum = NextState.main_frame.inners().size();

								int edgePercent = NextState.main_frame.getNewPieceEdgePercent(NowState.main_frame);

								NextState.score = edgePercent - holeNum * 10;

							}
							break;
						default:
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2
								- NextState.main_frame.inners().size() * 100;
							break;
						}

						if (((NowState.main_frame.perimeter() + NextState.main_frame.pre_perimeter() - NextState.main_frame.perimeter()) / 2) / NextState.main_frame.pre_perimeter() > 0.99) {
							NextState.score += 100000;
						}

						HStates[t + 1].push(NextState);
					}
				}
			}
		}
	}

	for (int i = 0; i < MaxTurn; i++) {
		if (HStates[i].size() >= 1) {
			nextState.push_back(HStates[i].top());
		}
	}

	for (auto & state : nextState) {
		if (state.main_frame.inners().size() == 0) {
			answer = state;
			return 1;
		}
	}

	return 0;
}

//図形中最小辺。つかわないんだよなあ
double min_side_cal(std::vector<std::array<int, 2>> figure) {
	double side = 0.0;
	int x, y;
	double min_side = 50000;

	for (auto i = figure.begin(); i != figure.end() - 1; i++) {
		x = i[0][0] - (i + 1)[0][0];
		y = i[0][1] - (i + 1)[0][1];
		side = std::pow(x, 2) + std::pow(y, 2);
		if (side < min_side)min_side = side;
	}

	return min_side;
}

//図形中最小角度。同上
double min_degree_cal(std::vector<std::array<int, 2>> figure) {
	std::array<int, 2> second_point;
	int count = 1;
	for (auto i : figure) {
		if (count == 2) {
			second_point = { i[0],i[1] };
			break;
		}
		count++;
	}
	figure.push_back(second_point);

	double first_degree;
	double second_degree;

	int x, y;

	double temporary_degree;
	double min_deg = 4;

	for (auto i = figure.begin(); i != figure.end() - 2; i++) {
		x = i[0][0] - (i + 1)[0][0];
		y = i[0][1] - (i + 1)[0][1];
		first_degree = std::atan2(y, x);
		if (first_degree < 0)first_degree += 2 * Puzzle::M_PI;

		x = (i + 2)[0][0] - (i + 1)[0][0];
		y = (i + 2)[0][1] - (i + 1)[0][1];
		second_degree = std::atan2(y, x);
		if (second_degree < 0)second_degree += 2 * Puzzle::M_PI;

		if (first_degree < second_degree) {
			second_degree -= 2 * Puzzle::M_PI;
		}

		temporary_degree = first_degree - second_degree;

		if (temporary_degree < min_deg)min_deg = temporary_degree;

	}

	return min_deg;
}

//判定部
bool exclude_judge(std::vector<std::array<int, 2>> ex_fream, double piece_min_side, double piece_min_degree) {
	double fream_min_side = min_side_cal(ex_fream);
	double fream_min_degree = min_degree_cal(ex_fream);

	if (fream_min_degree < piece_min_degree || fream_min_side < piece_min_side) {
		//最小角度が上回る、もしくわ最小辺が上回る
		return true;
	}
	else return false;
}

//後輩に作ってもらった、あんまり使わなかった。ありがとう。ごめんね。
//引数に対して除外を実行してください。
//一番最初に一回呼び出しておく
int Puzzle::excludeState(std::vector<Puzzle::state> & states) {


	static double piece_min_side = 1000 * 1000;
	static double piece_min_degree = 7;

	if (piece_min_side == 1000 * 1000) {

		//残りピース情報を取得
		for (auto& first_states : states) {
			for (auto& p_info : first_states.pieces) {
				polygon_type& piece = piece_manager::get_instance().getPolygon(p_info.piece_num);
				for (int i = 0; i < piece.outer().size() - 1; i++) {
					piece_min_degree = std::min(piece_min_degree, Puzzle::getInteriorAngle(piece, i));
					piece_min_side = std::min(piece_min_side, boost::geometry::distance(piece.outer()[i], piece.outer()[i + 1]));
				}
			}
		}

		return 0;
	}

	int state_number = 0;
	int erase_number = 0;

	//それぞれのstateに対して判定を行う
	for (int k = 0; k < states.size(); k++) {
		auto& state_i = states[k];
		//stateには1つのフレームが存在する
		//が、穴の数は複数個ある
		//フレーム(穴)の総数だけ回す
		for (int i = 0; i < state_i.main_frame.inners().size(); i++) {

			double frame_min_side = 1000 * 1000;
			double frame_min_degree = 7;

			// i 個めのフレームの頂点数分回す
			for (int j = 0; j < state_i.main_frame.inners()[i].size() - 1; j++) {
				frame_min_degree = std::min(frame_min_degree, Puzzle::getExternalAngle(state_i.main_frame, i, j));
				frame_min_side = std::min(frame_min_side, boost::geometry::distance(state_i.main_frame.inners()[i][j], state_i.main_frame.inners()[i][j + 1]));

			}
			//f_freamにはフレーム頂点情報
			//以下判定処理
			if (frame_min_side < piece_min_side || frame_min_degree < piece_min_degree) {//frame_min_side < piece_min_side || frame_min_degree < piece_min_degree) {
				//ダメな場合、このstateごと消す
				//消し方分からん…(´・ω・｀)
				states.erase(states.begin() + state_number - erase_number);
				erase_number++;
				//次のstateへ行く
				break;
			}
		}
		state_number++;
	}



	return 0;
}

//あきらめの全探索。半分ネタ。
//全探索の方式はchokudaiサーチ。というか深さ。
int Puzzle::beam_search::fullSearchBySuzuki(std::vector<Puzzle::state>& nowState, std::vector<Puzzle::state>& nextState, int funcEnum, int funcEval, Puzzle::state& answer, std::mutex& mtx, int & progressShare)
{

	const int MaxTurn = 50;
	std::unordered_map<unsigned long long int, std::vector<state>> hash;

	std::priority_queue<state> HStates[51];

	for (int i = 0; i < nowState.size(); i++) {
		HStates[0].push(nowState[i]);
	}

	int ChokudaiWidth = 1;

	//秒数でやるのが一般的だがあえてここではこうしている。
	//結果によっては切り替える。

	int loop = 0;

	while (true) {

		mtx.lock();
		if (progressShare == -1) {
			mtx.unlock();
			break;
		}
		else {
			progressShare = loop;
			mtx.unlock();
			loop++;
		}

		for (size_t t = 0; t < MaxTurn; t++) {
			//Puzzle::file_manager::output_error(std::to_string(t) + ":" + std::to_string(HStates[t].size()));
			for (size_t i = 0; i < ChokudaiWidth; i++) {
				if (HStates[t].size() <= 0) break;
				auto NowState = HStates[t].top();
				nextState.push_back(NowState);
				HStates[t].pop();

				//funcEnumによって関数を切り替える。
				std::vector<Puzzle::state> NextStates;

				switch (funcEnum)
				{
				case 0://all
					NextStates = NowState.getNextState();
					break;
				case 1://deg
					NextStates = NowState.getNextStateDeg();
					break;
				case 2://deg plus
					NextStates = NowState.getNextStateDegPlus();
					break;
				case 3://side
					NextStates = NowState.getNextStateSide();
					break;
				case 4://side plus
					NextStates = NowState.getNextStateSidePlus();
					break;
				case 5://side deg plus
					NextStates = NowState.getNextStateEval();
					break;
				default://all
					NextStates = NowState.getNextState();
					break;
				}

				for (auto& NextState : NextStates) {

					bool flag = true;

					unsigned long long int temporaryStateHashKey = NextState.getHash();

					for (int j = 0; j < hash[temporaryStateHashKey].size(); j++) {
						if (hash[temporaryStateHashKey][j] == NextState) {
							flag = false;
							break;
						}
					}


					if (flag) {

						if (NextState.main_frame.inners().size() == 0) {
							answer = NextState;
							return 1;
						}

						//答えになる可能性がないならスキップする
						if (!NextState.isAnswerPossibility()) {
							continue;
						}


						//組み付けるかつ答えになりえないならcontinue;

						bool isAnswer = true;

						while (true) {

							bool f = true;
							for (int arrange_num = 0; arrange_num < NextState.pieces.size(); arrange_num++) {

								if (NextState.pieces[arrange_num].piece_arrange == false) {
									continue;
								}

								Puzzle::frame result;
								//ここですでに重なることは考慮しない
								const int arrange_result = Puzzle::unionByArrange(NextState.main_frame, piece_manager::get_instance().getData(NextState.pieces[arrange_num].piece_num).rotation[0], result);

								if (arrange_result == 0) {
									NextState.main_frame = result;
									NextState.pieces.erase(NextState.pieces.begin() + arrange_num);
									arrange_num--;
									f = false;
								}
								else if (arrange_result == 2) {
									isAnswer = false;
								}
							}
							if (f)
								break;
						}

						if (!isAnswer) {
							continue;
						}


						hash[temporaryStateHashKey].push_back(NextState);

						//funcEvalによって関数を切り替える。

						switch (funcEval)
						{
						case 0://Edge
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2;
							break;
						case 1://EdgePlus
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2;
							break;
						case 2://EdgePercentPlus
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) * 100 / (2 * NextState.main_frame.pre_perimeter()) + NowState.score / 10;
							break;
						case 3://EdgePlus&Hole
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2
								- NextState.main_frame.inners().size() * 100;
							break;
						case 4://None
							NextState.score = 0;
							break;
						case 5://超強力な除外機能を持たせる。
							if (!NextState.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
								continue;
							}
							if (!NextState.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
								continue;
							}

							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2;

							break;
						case 6://ByKitajima
							if (true) {
								if (!NextState.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
									continue;
								}
								if (!NextState.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
									continue;
								}

								int holeNum = NextState.main_frame.inners().size();

								int edgePercent = (NowState.main_frame.perimeter()
									+ NextState.main_frame.pre_perimeter()
									- NextState.main_frame.perimeter()) * 100 / (2 * NextState.main_frame.pre_perimeter());

								NextState.score = edgePercent*edgePercent - holeNum * 5;

							}
							break;
						case 7://ByKitajima2 getNewPieceEdgePercent回りでのデバッグをしないと使用禁止
							if (true) {
								if (!NextState.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
									continue;
								}
								if (!NextState.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
									continue;
								}

								int holeNum = NextState.main_frame.inners().size();

								int edgePercent = NextState.main_frame.getNewPieceEdgePercent(NowState.main_frame);

								NextState.score = edgePercent - holeNum * 10;

							}
							break;
						default:
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2
								- NextState.main_frame.inners().size() * 100;
							break;
						}

						if (((NowState.main_frame.perimeter() + NextState.main_frame.pre_perimeter() - NextState.main_frame.perimeter()) / 2) / NextState.main_frame.pre_perimeter() > 0.99) {
							NextState.score += 100000;
						}

						HStates[t + 1].push(NextState);
					}
				}
			}
		}
	}

	return 0;
}


/*
動かしたときどういった状態を返すかが問題なんだよなあ。




分割されたフレームの内側の最小のところから埋めていく。埋められなかったらその置き方をやめる探索方法。（中盤）
//size >= 2ならstd::make_pair(area,i),sort一つも置けなかったらあれするのは、探索方針かな。てか新状態出さないだけで十分かも
一番小さい辺から入れていく（９０、２７０度は除外、５まで前後）、一番小さい角度から埋めていく。（５まで前後）
//その方針の上で、小さい角度から入れていく。（９０、２７０度は除外）５個出るまでやる。９０度まで
//その方針の上で、小さい辺から入れていく。５個出るまでやる。９０度まで
一番大きい角度から埋めていく。（なぞ）（やるとしても５前後、９０、２７０度は除外）
//上から５個だけやる
一番大きい辺から埋めていく。（なぞ）（５前後？これははてな）
//上から５個だけやる
//get_state_size = 5;


*/
//試行錯誤中に大会になってしまったエース期待のやつ。現状ではフルサーチとマジでほとんど一緒。
//評価値関数のテスト中だった。
int Puzzle::beam_search::beamStackSearchBySuzuki(std::vector<Puzzle::state>& nowState, std::vector<Puzzle::state>& nextState, int funcEnum, int funcEval, Puzzle::state& answer, std::mutex& mtx, int & progressShare)
{

	//様々な状態の上から５個見る
	const int GET_STATE_SIZE = 5;

	const int MaxTurn = 50;

	//状態の引継ぎは考える
	std::unordered_map<unsigned long long int, std::vector<state>> hash;

	std::priority_queue<state> HStates[51];

	for (int i = 0; i < nowState.size(); i++) {
		HStates[0].push(nowState[i]);
	}

	int ChokudaiWidth = 2;

	//秒数でやるのが一般的だがあえてここではこうしている。
	//結果によっては切り替える。
	const int WIDTH = 10;

	int redo = 0;
	while (WIDTH > ++redo) {

		mtx.lock();
		if (progressShare == -1) {
			mtx.unlock();
			break;
		}
		else {
			progressShare = 100.0 * redo / WIDTH;
			mtx.unlock();
		}



		for (size_t t = 0; t < MaxTurn; t++) {
			//Puzzle::file_manager::output_error(std::to_string(t) + ":" + std::to_string(HStates[t].size()));
			for (size_t i = 0; i < ChokudaiWidth; i++) {
				if (HStates[t].size() <= 0) break;
				auto NowState = HStates[t].top();
				nextState.push_back(NowState);
				HStates[t].pop();

				//funcEnumによって関数を切り替える。
				std::vector<Puzzle::state> NextStates;

				//一旦、関数のテストのみにする
				NextStates = NowState.getNextStateEval();

				for (auto& NextState : NextStates) {

					bool flag = true;

					unsigned long long int temporaryStateHashKey = NextState.getHash();

					for (int j = 0; j < hash[temporaryStateHashKey].size(); j++) {
						if (hash[temporaryStateHashKey][j] == NextState) {
							flag = false;
							break;
						}
					}


					if (flag) {


						//答えになる可能性がないならスキップする
						if (!NextState.isAnswerPossibility()) {
							continue;
						}


						//組み付けるかつ答えになりえないならcontinue;

						bool isAnswer = true;

						while (true) {

							bool f = true;
							for (int arrange_num = 0; arrange_num < NextState.pieces.size(); arrange_num++) {

								if (NextState.pieces[arrange_num].piece_arrange == false) {
									continue;
								}

								Puzzle::frame result;
								//ここですでに重なることは考慮しない
								const int arrange_result = Puzzle::unionByArrange(NextState.main_frame, piece_manager::get_instance().getData(NextState.pieces[arrange_num].piece_num).rotation[0], result);

								if (arrange_result == 0) {
									NextState.main_frame = result;
									NextState.pieces.erase(NextState.pieces.begin() + arrange_num);
									arrange_num--;
									f = false;
								}
								else if (arrange_result == 2) {
									isAnswer = false;
								}
							}
							if (f)
								break;
						}

						if (!isAnswer) {
							continue;
						}


						hash[temporaryStateHashKey].push_back(NextState);

						//funcEvalによって関数を切り替える。

						switch (funcEval)
						{
						case 0://Edge
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2;
							break;
						case 1://EdgePlus
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2;
							break;
						case 2://EdgePercentPlus
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) * 100 / (2 * NextState.main_frame.pre_perimeter()) + NowState.score / 10;
							break;
						case 3://EdgePlus&Hole
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2
								- NextState.main_frame.inners().size() * 100;
							break;
						case 4://None
							NextState.score = 0;
							break;
						case 5://超強力な除外機能を持たせる。
							if (!NextState.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
								continue;
							}
							if (!NextState.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
								continue;
							}

							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2;

							break;
						case 6://ByKitajima
							if (true) {
								if (!NextState.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
									continue;
								}
								if (!NextState.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
									continue;
								}

								int holeNum = NextState.main_frame.inners().size();

								int edgePercent = (NowState.main_frame.perimeter()
									+ NextState.main_frame.pre_perimeter()
									- NextState.main_frame.perimeter()) * 100 / (2 * NextState.main_frame.pre_perimeter());

								NextState.score = edgePercent*edgePercent - holeNum * 5;

							}
							break;
						case 7://ByKitajima2 getNewPieceEdgePercent回りでのデバッグをしないと使用禁止
							if (true) {
								if (!NextState.equalEdge()) {//辺的にフレームの全ての辺に対応するピースがあるならfalse
									continue;
								}
								if (!NextState.equalDegree()) {//角度的にフレームの全ての頂点に対応するピースがあるならtrue
									continue;
								}

								int holeNum = NextState.main_frame.inners().size();

								int edgePercent = NextState.main_frame.getNewPieceEdgePercent(NowState.main_frame);

								NextState.score = edgePercent - holeNum * 10;

							}
							break;
						default:
							NextState.score = (NowState.main_frame.perimeter()
								+ NextState.main_frame.pre_perimeter()
								- NextState.main_frame.perimeter()) / 2 + NowState.score / 2
								- NextState.main_frame.inners().size() * 100;
							break;
						}

						if (((NowState.main_frame.perimeter() + NextState.main_frame.pre_perimeter() - NextState.main_frame.perimeter()) / 2) / NextState.main_frame.pre_perimeter() > 0.99) {
							NextState.score += 100000;
						}

						HStates[t + 1].push(NextState);
					}
				}
			}
		}
	}

	for (int i = 0; i < MaxTurn; i++) {
		if (HStates[i].size() >= 1) {
			nextState.push_back(HStates[i].top());
		}
	}

	for (auto & state : nextState) {
		if (state.main_frame.inners().size() == 0) {
			answer = state;
			return 1;
		}
	}

	return 0;
}

