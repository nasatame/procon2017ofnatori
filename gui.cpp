#include "gui.h"

bool Puzzle::gui::displayCamera()
{
	Webcam webcam;

	// デバイス ID: 0 の Web カメラを起動
	if (!webcam.open(0, Size(640*2, 480*2)))
	{
		// 失敗したら終了
		return false;
	}

	// 撮影開始
	if (!webcam.start())
	{
		return false;
	}

	// 空の動的テクスチャ
	DynamicTexture texture;

	while (System::Update())
	{
		// カメラが新しい画像を撮影したら
		if (webcam.hasNewFrame())
		{
			// 動的テクスチャをカメラの画像で更新
			webcam.getFrame(texture);
		}

		if (texture)
		{
			texture.draw();
		}

		if (Input::KeyQ.clicked) {
			webcam.close();
			return true;
		}
	}
	webcam.close();
	return false;
}

//エラー出力　error_zbar.txt困ったら見て
//QRコードをデコードしてテキストファイルに出力する。
//使用する際はexeファイルから見てzbarinmg.exeをzbar/zbarimg.exeという風に設置しておくこと。（dllも必要）
//ファイル名も含め指定すること
//成功　CanDecodeQr(画像ファイルパス,出力ファイルパス);
bool CanDecodeQr(std::string img_file_path, std::string output_file_path)
{

	int ret = system(("zbarimg.lnk \"" + FileSystem::CurrentPath().narrow()  + img_file_path + '"' + " > " + output_file_path + " 2> error_zbar.txt").c_str());
	
	Println(L"return : " , ret);

	return ret == 0;
}


bool Puzzle::gui::takePicture()
{
	Webcam webcam;

	// デバイス ID: 0 の Web カメラを起動
	if (!webcam.open(1, Size(640*2, 480*2)))
	{
		// 失敗したら終了
		return false;
	}

	// 撮影開始

	if (!webcam.start())
	{
		return false;
	}

	// 空の動的テクスチャ
	DynamicTexture texture;
	// 空の画像
	Image image;
	//今まで読み込んだデータを保管する。
	s3d::String str;

	int i = 0;

	while (System::Update())
	{
		// カメラが新しい画像を撮影したら
		if (webcam.hasNewFrame())
		{
			// 動的テクスチャをカメラの画像で更新
			webcam.getFrame(texture);
		}

		if (texture)
		{
			texture.draw();
		}

		if (Input::KeyQ.clicked) {
			webcam.close();
			return true;
		}

		if (Input::KeyEnter.clicked) {
			webcam.getFrame(image);
			image.save(L"p2017.jpg");

			//Siv3Dのデフォルトのバーコードリーダーが精度が安定しなかったためZbarに切り替えた。

			bool ret = CanDecodeQr("p2017.jpg", "cameraData" + std::to_string(++i) + ".txt");

			if (!ret)i--;
		}

		font(str).draw();

	}

	webcam.close();

	return false;
}

int Puzzle::gui::takeInformationPicture()
{

	Webcam webcam;

	// デバイス ID: 0 の Web カメラを起動
	if (!webcam.open(1, Size(640*2, 480*2)))
	{
		// 失敗したら終了
		return -1;
	}

	// 撮影開始

	if (!webcam.start())
	{
		return -1;
	}

	// 空の動的テクスチャ
	DynamicTexture texture;
	// 空の画像
	Image image;
	//今まで読み込んだデータを保管する。
	s3d::String str;

	int i = 0;

	while (System::Update())
	{
		// カメラが新しい画像を撮影したら
		if (webcam.hasNewFrame())
		{
			// 動的テクスチャをカメラの画像で更新
			webcam.getFrame(texture);
		}

		if (texture)
		{
			texture.draw();
		}

		if (Input::KeyQ.clicked) {
			webcam.close();
			break;
		}

		if (Input::KeyEnter.clicked) {
			webcam.getFrame(image);
			image.save(L"p2017_information.jpg");

			//Zbarに丸投げ

			bool ret = CanDecodeQr("p2017_information.jpg", "cameraDataInfo" + std::to_string(++i) + ".txt");

			if (!ret)i--;
		}

		font(str).draw();

	}

	webcam.close();

	return i;
}

bool Puzzle::gui::takePicture(String file_name)
{

	Webcam webcam;

	// デバイス ID: 0 の Web カメラを起動
	if (!webcam.open(0, Size(640*2, 480*2)))
	{
		// 失敗したら終了
		return false;
	}

	// 撮影開始
	if (!webcam.start())
	{
		return false;
	}

	// 空の動的テクスチャ
	DynamicTexture texture;
	// 空の画像
	Image image;
	//今まで読み込んだデータを保管する。
	s3d::String str;

	int i = 0;

	while (System::Update())
	{
		// カメラが新しい画像を撮影したら
		if (webcam.hasNewFrame())
		{
			// 動的テクスチャをカメラの画像で更新
			webcam.getFrame(texture);
		}

		if (texture)
		{
			texture.draw();
		}

		if (Input::KeyQ.clicked) {
			webcam.close();
			return true;
		}

		if (Input::KeyEnter.clicked) {
			webcam.getFrame(image);
			image.save(L"p2017.jpg");

			QRData data;

			if (QR::Decode(image, data)) {

				str = data.text;

				TextWriter writer( file_name, s3d::OpenMode::Trunc, s3d::TextEncoding::ANSI);

				writer.writeln(L"QR-Code:" + data.text);

				writer.close();

			}

		}

		font(str).draw();

	}

	webcam.close();


	return false;
}
