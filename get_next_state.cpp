#include "beam_search.h"

//ここら辺おんなじコード多すぎ。○○コード

//標準関数使え
int distanceSquare(Puzzle::polygon_type::point_type a, Puzzle::polygon_type::point_type b) {
	return pow(a.x() - b.x(), 2) + pow(a.y() - b.y(), 2);
}

//微妙、列挙できる状態が少なすぎる。いちいち呼び出すのが面倒。よってお蔵入り
std::vector<Puzzle::state> Puzzle::state::getNextStateOnly() {

	std::vector<state> result;

	std::map<int, int> m;

	for (unsigned k = 0; k < main_frame.inners().size(); k++) {
		for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
			m[
				distanceSquare(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
			]++;
		}
	}

	for (unsigned s = 0; s < pieces.size(); s++) {
		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
			m[
				distanceSquare(piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j]
					, piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1])
			]++;
		}
	}

	for (unsigned k = 0; k < main_frame.inners().size(); k++) {
		for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
			int flag = 0;
			auto res = state();

			for (unsigned s = 0; s < pieces.size(); s++) {
				std::vector<piece_info> p = pieces;
				p.erase(p.begin() + s);
				for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/

					if (m[
						distanceSquare(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					] > 2) continue;

					if (equalDistance(piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
						piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1],
						main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {
						Puzzle::frame t;
						if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
							flag++;
							res = state(t, p);

							if (flag >= 2) {
								break;
							}
						}
					}
				}
			}


			//flagが１のままだったら（この辺に対してはまるものがひとつしかなかったら）resultにわたす
			if (flag == 1) {
				result.push_back(res);
			}
		}
	}

	for (unsigned k = 0; k < main_frame.inners().size(); k++) {
		for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
			int flag = 0;
			auto res = state();

			for (unsigned s = 0; s < pieces.size(); s++) {
				std::vector<piece_info> p = pieces;
				p.erase(p.begin() + s);
				pieces[s].ope.reverseFlag = true;
				for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size() - 1; j++) {
					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/

					if (m[
						distanceSquare(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					] > 2) continue;

					if (equalDistance(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j],
						piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j + 1],
						main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {
						Puzzle::frame t;
						if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
							flag++;
							res = state(t, p);

							if (flag >= 2) {
								break;
							}

						}
					}
				}
				pieces[s].ope.reverseFlag = false;
			}

			//flagが１のままだったら（この辺に対してはまるものがひとつしかなかったら）resultにわたす
			if (flag == 1) {
				result.push_back(res);
			}
		}
	}

	for (unsigned k = 0; k < main_frame.inners().size(); k++) {
		for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
			int flag = 0;
			auto res = state();

			for (unsigned s = 0; s < pieces.size(); s++) {
				std::vector<piece_info> p = pieces;
				p.erase(p.begin() + s);
				for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
						)
					{
						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							flag++;
							res = state(t, p);

							if (flag >= 2) {
								break;
							}

						}
					}
				}
			}


			//flagが１のままだったら（この辺に対してはまるものがひとつしかなかったら）resultにわたす
			if (flag == 1) {
				result.push_back(res);
			}
		}
	}

	for (unsigned k = 0; k < main_frame.inners().size(); k++) {
		for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
			int flag = 0;
			auto res = state();

			for (unsigned s = 0; s < pieces.size(); s++) {
				std::vector<piece_info> p = pieces;
				p.erase(p.begin() + s);
				pieces[s].ope.reverseFlag = true;
				for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size() - 1; j++) {
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001

						)
					{
						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							flag++;
							res = state(t, p);

							if (flag >= 2) {
								break;
							}
						}
					}

				}
				pieces[s].ope.reverseFlag = false;
			}

			//flagが１のままだったら（この辺に対してはまるものがひとつしかなかったら）resultにわたす
			if (flag == 1) {
				result.push_back(res);
			}
		}
	}

	return result;
}



std::vector<Puzzle::state> Puzzle::state::getNextStateDegPlus()
{
	std::vector<Puzzle::state> result;

	//内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size(); j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size(); o++) {

					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
						)
					{
						if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - M_PI / 2) < 0.000000001)continue;
						if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - M_PI) < 0.000000001)continue;
						if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - 3 * M_PI / 2) < 0.000000001)continue;
						if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - M_PI / 2) < 0.000000001)continue;
						if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - M_PI) < 0.000000001)continue;
						if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - 3 * M_PI / 2) < 0.000000001)continue;

						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}


	}

	//反転後内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {
		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;


		for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size(); j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size(); o++) {
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001

						)
					{
						if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - M_PI / 2) < 0.000000001)continue;
						if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - M_PI) < 0.000000001)continue;
						if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - 3 * M_PI / 2) < 0.000000001)continue;
						if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - M_PI / 2) < 0.000000001)continue;
						if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - M_PI) < 0.000000001)continue;
						if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - 3 * M_PI / 2) < 0.000000001)continue;

						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}

		pieces[s].ope.reverseFlag = false;

	}

	return result;
}


//unionByDeg対応
std::vector<Puzzle::state> Puzzle::state::getNextState()
{

	std::vector<state> result;

	//同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/
					if (equalDistance(piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
						piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1],
						main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {
						Puzzle::frame t;
						if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}


	}

	//反転後同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size() - 1; j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/
					if (equalDistance(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j],
						piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j + 1],
						main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {
						Puzzle::frame t;
						if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}

		pieces[s].ope.reverseFlag = false;

	}

	//内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {


		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size(); j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size(); o++) {
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
						)
					{
						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}


	}

	//反転後内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;


		for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size(); j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size(); o++) {
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001

						)
					{
						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}

		pieces[s].ope.reverseFlag = false;

	}

	return result;
}

std::vector<Puzzle::state> Puzzle::state::getNextStateDeg()
{

	std::vector<state> result;

	//内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size(); j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size(); o++) {
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
						)
					{
						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}


	}

	//反転後内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;


		for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size(); j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size(); o++) {
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001

						)
					{
						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}

		pieces[s].ope.reverseFlag = false;

	}

	return result;

}

std::vector<Puzzle::state> Puzzle::state::getNextStateSide()
{
	std::vector<state> result;

	//同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/
					if (equalDistance(piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
						piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1],
						main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {
						Puzzle::frame t;
						if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}


	}

	//反転後同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size() - 1; j++) {
			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/
					if (equalDistance(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j],
						piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j + 1],
						main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {
						Puzzle::frame t;
						if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}

		pieces[s].ope.reverseFlag = false;

	}

	return result;
}

std::vector<Puzzle::state> Puzzle::state::getNextStateSidePlus()
{
	std::vector<state> result;

	//同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {

			if (abs(boost::geometry::distance(
				piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
				piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1])
				-
				round(boost::geometry::distance(
					piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
					piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1])))
				< 0.000000000001
				) continue;

			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {

					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/


					if (abs(boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
						-
						round(boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])))
						< 0.00001
						) continue;

					if (equalDistance(piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
						piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1],
						main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {

						Puzzle::frame t;
						if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}
		}


	}

	//反転後同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size() - 1; j++) {


			if (abs(boost::geometry::distance(
				piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
				piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1])
				-
				round(boost::geometry::distance(
					piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
					piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1])))
				< 0.00001
				) continue;

			for (unsigned k = 0; k < main_frame.inners().size(); k++) {
				for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
					/*
					if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
					boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					) {*/


					if (abs(boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
						-
						round(boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])))
						< 0.00001
						) continue;

					if (equalDistance(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j],
						piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j + 1],
						main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {
						Puzzle::frame t;
						if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}

			}
		}

		pieces[s].ope.reverseFlag = false;

	}

	return result;
}


//GUIを用いた探索用の関数。
std::vector<Puzzle::state> Puzzle::state::getNextStateSelectPoint(int fnum, int pnum)
{
	std::vector<state> result;

	//内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size(); j++) {
			if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, fnum, pnum)) < 0.000000000001
				||
				abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, fnum, pnum)) - M_PI * 2) < 0.000000000001
				||
				abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, fnum, pnum)) - M_PI * 2) < 0.000000000001
				||
				abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, fnum, pnum)) < 0.000000000001
				)
			{
				Puzzle::frame t;
				if (Puzzle::unionByDeg(main_frame, pieces[s], fnum, pnum, j, t)) {
					result.push_back(state(t, p));
				}
			}

		}


	}

	//反転後内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;


		for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size(); j++) {

			if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, fnum, pnum)) < 0.000000000001
				||
				abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, fnum, pnum)) - M_PI * 2) < 0.000000000001
				||
				abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, fnum, pnum)) < 0.000000000001
				||
				abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, fnum, pnum)) - M_PI * 2) < 0.000000000001

				)
			{
				Puzzle::frame t;
				if (Puzzle::unionByDeg(main_frame, pieces[s], fnum, pnum, j, t)) {
					result.push_back(state(t, p));
				}
			}

		}

		pieces[s].ope.reverseFlag = false;

	}

	return result;
}



//GUIを用いた探索用の関数。
std::vector<Puzzle::state> Puzzle::state::getNextStateSelectEdge(int fnum, int pnum)
{

	std::vector<state> result;

	//同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {
			if (equalDistance(piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
				piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1],
				main_frame.inners()[fnum][pnum], main_frame.inners()[fnum][pnum + 1])) {
				Puzzle::frame t;
				if (Puzzle::unionBySide(main_frame, pieces[s], fnum, pnum, j, t)) {
					result.push_back(state(t, p));
				}
			}
		}


	}

	//反転後同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size() - 1; j++) {
			if (equalDistance(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j],
				piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j + 1],
				main_frame.inners()[fnum][pnum], main_frame.inners()[fnum][pnum + 1])) {
				Puzzle::frame t;
				if (Puzzle::unionBySide(main_frame, pieces[s], fnum, pnum, j, t)) {
					result.push_back(state(t, p));
				}
			}
		}

		pieces[s].ope.reverseFlag = false;

	}

	return result;
}


std::vector<Puzzle::state> Puzzle::state::getNextStateEval()
{

	//まず、枠内の最小を求める
	int innner_minimize_num = 0;
	int innenr_minimize_size = 1000 * 1000;

	for (unsigned k = 0; k < main_frame.inners().size(); k++) {
		if (-boost::geometry::area(main_frame.inners()[k]) <= innenr_minimize_size) {
			innenr_minimize_size = -boost::geometry::area(main_frame.inners()[k]);
			innner_minimize_num = k;
		}
	}


	std::vector<Puzzle::state> result;


	//SidePlus部分
	//同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size() - 1; j++) {

			if (abs(boost::geometry::distance(
				piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
				piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1])
				-
				round(boost::geometry::distance(
					piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
					piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1])))
				< 0.000000000001
				) continue;

			const int k = innner_minimize_num;
			for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {

				/*
				if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
				boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
				) {*/


				if (abs(boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					-
					round(boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])))
					< 0.00001
					) continue;

				if (equalDistance(piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
					piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1],
					main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {

					Puzzle::frame t;
					if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
						result.push_back(state(t, p));
					}
				}
			}
		}


	}

	//反転後同じ辺の長さで合成
	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size() - 1; j++) {


			if (abs(boost::geometry::distance(
				piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
				piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1])
				-
				round(boost::geometry::distance(
					piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j],
					piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer()[j + 1])))
				< 0.00001
				) continue;

			const int k = innner_minimize_num;
			for (unsigned o = 0; o < main_frame.inners()[k].size() - 1; o++) {
				/*
				if (boost::geometry::distance(pieces[s].outer()[j], pieces[s].outer()[j + 1]) ==
				boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
				) {*/


				if (abs(boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])
					-
					round(boost::geometry::distance(main_frame.inners()[k][o], main_frame.inners()[k][o + 1])))
					< 0.00001
					) continue;

				if (equalDistance(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j],
					piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer()[j + 1],
					main_frame.inners()[k][o], main_frame.inners()[k][o + 1])) {
					Puzzle::frame t;
					if (Puzzle::unionBySide(main_frame, pieces[s], k, o, j, t)) {
						result.push_back(state(t, p));
					}
				}
			}

		}

		pieces[s].ope.reverseFlag = false;

	}

	//SidePlus部分終了

	//DegPlus部分開始


	//内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {

		if (pieces[s].piece_arrange) continue;

		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);

		for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size(); j++) {

			const int k = innner_minimize_num;

			for (unsigned o = 0; o < main_frame.inners()[k].size(); o++) {

				if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
					||
					abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
					||
					abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
					||
					abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
					)
				{
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - M_PI / 2) < 0.000000001)continue;
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - M_PI) < 0.000000001)continue;
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - 3 * M_PI / 2) < 0.000000001)continue;
					if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - M_PI / 2) < 0.000000001)continue;
					if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - M_PI) < 0.000000001)continue;
					if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - 3 * M_PI / 2) < 0.000000001)continue;

					Puzzle::frame t;
					if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
						result.push_back(state(t, p));
					}
				}
			}
		}


	}

	//反転後内角と外角で引くと０度になる角度で合成

	for (unsigned s = 0; s < pieces.size(); s++) {
		std::vector<piece_info> p = pieces;
		p.erase(p.begin() + s);
		pieces[s].ope.reverseFlag = true;


		for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size(); j++) {
			const int k = innner_minimize_num;


			for (unsigned o = 0; o < main_frame.inners()[k].size(); o++) {
				if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
					||
					abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
					||
					abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
					||
					abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001

					)
				{
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - M_PI / 2) < 0.000000001)continue;
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - M_PI) < 0.000000001)continue;
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - 3 * M_PI / 2) < 0.000000001)continue;
					if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - M_PI / 2) < 0.000000001)continue;
					if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - M_PI) < 0.000000001)continue;
					if (abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - 3 * M_PI / 2) < 0.000000001)continue;

					Puzzle::frame t;
					if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
						result.push_back(state(t, p));
					}
				}
			}
		}

		pieces[s].ope.reverseFlag = false;

	}

	//DegPlus部分終了

	//size() == 0の場合Degする
	if (result.size() <= 0) {

		//内角と外角で引くと０度になる角度で合成

		for (unsigned s = 0; s < pieces.size(); s++) {

			if (pieces[s].piece_arrange) continue;

			std::vector<piece_info> p = pieces;
			p.erase(p.begin() + s);

			for (unsigned j = 0; j < piece_manager::get_instance().getPolygon(pieces[s].piece_num).outer().size(); j++) {
				const int k = innner_minimize_num;
				for (unsigned o = 0; o < main_frame.inners()[k].size(); o++) {
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygon(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
						)
					{
						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}


		}

		//反転後内角と外角で引くと０度になる角度で合成

		for (unsigned s = 0; s < pieces.size(); s++) {

			if (pieces[s].piece_arrange) continue;

			std::vector<piece_info> p = pieces;
			p.erase(p.begin() + s);
			pieces[s].ope.reverseFlag = true;


			for (unsigned j = 0; j < piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num).outer().size(); j++) {
				const int k = innner_minimize_num;
				for (unsigned o = 0; o < main_frame.inners()[k].size(); o++) {
					if (abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getExternalAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getInteriorAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001
						||
						abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) < 0.000000000001
						||
						abs(abs(Puzzle::getInteriorAngle(piece_manager::get_instance().getPolygonInvert(pieces[s].piece_num), j) - Puzzle::getExternalAngle(main_frame, k, o)) - M_PI * 2) < 0.000000000001

						)
					{
						Puzzle::frame t;
						if (Puzzle::unionByDeg(main_frame, pieces[s], k, o, j, t)) {
							result.push_back(state(t, p));
						}
					}
				}
			}

			pieces[s].ope.reverseFlag = false;

		}

	}

	return result;
}
