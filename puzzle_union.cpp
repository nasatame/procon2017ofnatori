#include "beam_search.h"

//ここが一番大変だった。
//高速なピースの合成関数。
//回転後の状態を全て列挙しておいて。それをframeに組みつけてる。
//ピースの回転情報はsingletonでグローバルに保存してある。
//たまにバグル。アンスレッドセーフにかかわらずメッチャスレッドで使ってる衆の中の一人。設計は気をつけよう


//argument= frame piece frame_inner_num vertex_A vertex_B
bool Puzzle::unionBySide(Puzzle::frame A, Puzzle::piece_info pi, int inum, int va, int vb, frame & result)
{


	std::pair<int, int> target(A.inners()[inum][va].y() - A.inners()[inum][va + 1].y(), A.inners()[inum][va].x() - A.inners()[inum][va + 1].x());
	piece B;

	if (pi.ope.reverseFlag) {//true
		std::unordered_map<double,piece>& rotation_data = piece_manager::get_instance().getData(pi.piece_num).invert_rotation;

		for (auto& rotate_piece : rotation_data) {
			if (target == std::pair<int, int>(rotate_piece.second.outer()[vb + 1].y() - rotate_piece.second.outer()[vb].y(), rotate_piece.second.outer()[vb + 1].x() - rotate_piece.second.outer()[vb].x())) {
				B = rotate_piece.second;
			}
		}

		if (B.outer().size() == 0) {
			return false;
		}

	}
	else {//false
		std::unordered_map<double, piece> & rotation_data = piece_manager::get_instance().getData(pi.piece_num).rotation;

		for (auto& rotate_piece : rotation_data) {
			if (target == std::pair<int, int>(rotate_piece.second.outer()[vb + 1].y() - rotate_piece.second.outer()[vb].y(), rotate_piece.second.outer()[vb + 1].x() - rotate_piece.second.outer()[vb].x())) {
				B = rotate_piece.second;
			}
		}

		if (B.outer().size() == 0) {
			return false;
		}
	}

	//移動
	//フレームは初期位置から動かすな！！注意。

	//最終的にA(1-2),B(1-2)が上手くくっつくように移動する。

	canMove(B, A.inners()[inum][va + 1].x() - B.outer()[vb].x(), A.inners()[inum][va + 1].y() - B.outer()[vb].y());

	//エラー制御とりあえず面積を見て足されたか確認すればOKかと思われる。
	//これを潜り抜けるパターンが存在することが分かった。

	std::vector<polygon_type_double> vec_poly;
	polygon_type_double Apoly = convertePolygon(A.polygon);
	polygon_type_double Bpoly = convertePolygon(B.polygon);

	boost::geometry::union_(Apoly, Bpoly, vec_poly);

	//図形数からエラーを検出する。
	if (vec_poly.size() == 1) {

	}
	else {
		return false;
	}

	//面積的な面からエラーを検出する。
	if (std::abs(boost::geometry::area(A.polygon) + boost::geometry::area(B.polygon) - boost::geometry::area(vec_poly[0]))<0.0000000001) {

	}
	else {
		return false;
	}

	boost::geometry::correct(vec_poly[0]);

	//返すピースのA,Bのshared_ptrに親となった二つのピースを登録する。

	result = frame(convertePolygon(vec_poly[0]), std::shared_ptr<frame>(new frame(A, true)), std::shared_ptr<piece>(new piece(B, true)));

	canSimply(result);

	return true;
}

bool Puzzle::unionByDeg(Puzzle::frame A, Puzzle::piece_info pi, int inum, int va, int vb, frame & result)
{

	piece B;

	if (pi.ope.reverseFlag) {//true
		std::unordered_map<double, piece> & rotation_data = piece_manager::get_instance().getData(pi.piece_num).invert_rotation;

		for (auto& rotate_piece : rotation_data) {
			if (divEqual(
				A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())].y() - A.inners()[inum][correctSuffix(va, A.inners()[inum].size())].y(),
				A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())].x() - A.inners()[inum][correctSuffix(va, A.inners()[inum].size())].x(),
				rotate_piece.second.outer()[correctSuffix(vb, rotate_piece.second.outer().size())].y() - rotate_piece.second.outer()[correctSuffix(vb + 1, rotate_piece.second.outer().size())].y(),
				rotate_piece.second.outer()[correctSuffix(vb, rotate_piece.second.outer().size())].x() - rotate_piece.second.outer()[correctSuffix(vb + 1, rotate_piece.second.outer().size())].x()
			)) {
				B = rotate_piece.second;
				//break;
			}
		}

		if (B.outer().size() == 0) {
			return false;
		}

	}
	else {//false
		std::unordered_map<double, piece> & rotation_data = piece_manager::get_instance().getData(pi.piece_num).rotation;

		for (auto& rotate_piece : rotation_data) {
			/*
			!divEqual(A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())].y() - A.inners()[inum][correctSuffix(va, A.inners()[inum].size())].y(),
			A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())].x() - A.inners()[inum][correctSuffix(va, A.inners()[inum].size())].x(),
			rotate_piece.second.outer()[correctSuffix(vb, rotate_piece.second.outer().size())].y() - rotate_piece.second.outer()[correctSuffix(vb + 1, rotate_piece.second.outer().size())].y(),
			rotate_piece.second.outer()[correctSuffix(vb, rotate_piece.second.outer().size())].x() - rotate_piece.second.outer()[correctSuffix(vb + 1, rotate_piece.second.outer().size())].x()
			*/

			if (divEqual(
				A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())].y() - A.inners()[inum][correctSuffix(va, A.inners()[inum].size())].y(),
				A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())].x() - A.inners()[inum][correctSuffix(va, A.inners()[inum].size())].x(),
				rotate_piece.second.outer()[correctSuffix(vb, rotate_piece.second.outer().size())].y() - rotate_piece.second.outer()[correctSuffix(vb + 1, rotate_piece.second.outer().size())].y(),
				rotate_piece.second.outer()[correctSuffix(vb, rotate_piece.second.outer().size())].x() - rotate_piece.second.outer()[correctSuffix(vb + 1, rotate_piece.second.outer().size())].x()
			)) {
				B = rotate_piece.second;
				//break;
			}
		}

		if (B.outer().size() == 0) {
			return false;
		}
	}

	//移動
	//フレームは初期位置から動かすな！！注意。

	//最終的にA(1-2),B(1-2)が上手くくっつくように移動する。

	canMove(B, A.inners()[inum][va].x() - B.outer()[vb].x(), A.inners()[inum][va].y() - B.outer()[vb].y());

	//エラー制御とりあえず面積を見て足されたか確認すればOKかと思われる。
	//これを潜り抜けるパターンが存在することが分かった。

	std::vector<polygon_type_double> vec_poly;
	polygon_type_double Apoly = convertePolygon(A.polygon);
	polygon_type_double Bpoly = convertePolygon(B.polygon);

	boost::geometry::union_(Apoly, Bpoly, vec_poly);

	//図形数からエラーを検出する。
	if (vec_poly.size() == 1) {

	}
	else {
		return false;
	}

	//面積的な面からエラーを検出する。
	if (std::abs(boost::geometry::area(A.polygon) + boost::geometry::area(B.polygon) - boost::geometry::area(vec_poly[0]))<0.000000001) {

	}
	else {
		return false;
	}

	boost::geometry::correct(vec_poly[0]);

	//返すピースのA,Bのshared_ptrに親となった二つのピースを登録する。

	result = frame(convertePolygon(vec_poly[0]), std::shared_ptr<frame>(new frame(A, true)), std::shared_ptr<piece>(new piece(B, true)));

	canSimply(result);

	return true;
}


//移動させないでそのままUnionするシリーズ。
//返り値が、
//0ならOK
//1ならそもそもくっつかない
//2なら面積的エラー
int Puzzle::unionByArrange(frame A, piece B, frame& result)
{
	std::vector<polygon_type_double> vec_poly;
	polygon_type_double Apoly = convertePolygon(A.polygon);
	polygon_type_double Bpoly = convertePolygon(B.polygon);

	boost::geometry::union_(Apoly, Bpoly, vec_poly);

	//図形数からエラーを検出する。
	if (vec_poly.size() == 1) {

	}
	else {
		return 1;
	}

	//面積的な面からエラーを検出する。
	if (std::abs(boost::geometry::area(A.polygon) + boost::geometry::area(B.polygon) - boost::geometry::area(vec_poly[0]))<0.0000000001) {

	}
	else {
		return 2;
	}

	boost::geometry::correct(vec_poly[0]);

	//返すピースのA,Bのshared_ptrに親となった二つのピースを登録する。

	result = frame(convertePolygon(vec_poly[0]), std::shared_ptr<frame>(new frame(A, true)), std::shared_ptr<piece>(new piece(B, true)));

	canSimply(result);

	return 0;
}