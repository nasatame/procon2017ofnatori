#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <fstream>
#include <string>
#include <chrono>
#include <ctime>
#include <iomanip>
#include "piece.h"
#include "frame.h"
#include <Siv3d.hpp>


namespace Puzzle {

	class gui {
	public:

		int gui_move_x = 10;
		int gui_move_y = 10;
		int gui_magnification = 10;

		Font font;

		gui() {
			font = Font(10);
			Window::Resize(1020, 660);
		}

		template<class T>
		void output(const T& p, double thickness = 5, Color c = Palette::White) {

			if (p.outer().size() == 0) {
				return;
			}

			for (unsigned i = 0; i < p.outer().size() - 1; i++) {
				Line(Vec2(p.outer()[i].x() * gui_magnification + gui_move_x, p.outer()[i].y() * gui_magnification + gui_move_y), Vec2(p.outer()[i + 1].x() * gui_magnification + gui_move_x, p.outer()[i + 1].y() * gui_magnification + gui_move_y)).draw(thickness, c);
			}

		}

		//複数のピースが渡された際の関数
		template<class T>
		void output(const std::vector<T>& vecpie, double thickness = 5, Color c = Palette::White) {

			for (const auto & a : vecpie) {
				output(a, thickness, c);
			}

		}

		//内側を埋めて表示する。
		template<class T>
		void outputFill(const T& p, Color c = Palette::White) {

			if (p.outer().size() == 0) {
				return;
			}

			Polygon poly;
			s3d::Array<Vec2> ls;

			// 
			for (unsigned i = p.outer().size() - 1; i >= 1; i--) {
				ls.push_back({ p.outer()[i - 1].x() * gui_magnification + gui_move_x, p.outer()[i - 1].y() * gui_magnification + gui_move_y });
				//Triangle(Vec2(p.outer()[i-1].x(), p.outer()[i-1].y()), Vec2(p.outer()[i - 2].x(), p.outer()[i - 2].y()), Vec2(p.outer()[i - 3].x(), p.outer()[i - 3].y())).draw(c);
			}

			//poly = ls.asPolygon();
			poly = Polygon(ls);
			poly.draw(c);
		}

		//複数のピースが渡された際の関数
		template<class T>
		void outputFill(const std::vector<T>& vecpie, Color c = Palette::White) {

			for (const auto & a : vecpie) {
				outputFill(a, c);
			}

		}

		//周辺長を求める
		template<class T>
		void outputWithPerimeter(const T p, double thickness = 5, Color c = Palette::White) {

			if (p.size() == 0) {
				return;
			}

			for (unsigned i = 0; i < p.size(); i++) {
				const auto& v = p[i].outer()[0];
				font(boost::geometry::perimeter(p[i])).draw(Vec2(v.x() * gui_magnification + 5 + gui_move_x, v.y() * gui_magnification + 5 + gui_move_y));
			}
		}

		//面積を求める
		template<class T>
		void outputWithArea(const T p, double thickness = 5, Color c = Palette::White) {

			if (p.size() == 0) {
				return;
			}

			for (unsigned i = 0; i < p.size(); i++) {
				const auto& v = p[i].outer()[0];
				font(boost::geometry::area(p[i])).draw(Vec2(v.x() * gui_magnification + 5 + gui_move_x, v.y() * gui_magnification + 5 + gui_move_y));
			}
		}


		//頂点番号も追加して表示する。
		template<class T>
		void outputWithNumber(const T p, double thickness = 5, Color c = Palette::White) {

			if (p.outer().size() == 0) {
				return;
			}

			output(p, thickness, c);

			for (unsigned i = 1; i < p.outer().size(); i++) {
				const auto& v = p.outer()[i];
				font(i).draw(Vec2(v.x() * gui_magnification + 5 + gui_move_x, v.y() * gui_magnification + 5 + gui_move_y));
			}
		}

		template<class T>
		void outputWithNumber(std::vector<T> vecpie, double thickness = 5, Color c = Palette::White) {
			for (const auto & a : vecpie) {
				outputWithNumber(a, thickness, c);
			}
		}

		//頂点の内角を追加して表示する。
		template<class T>
		void outputWithInteriorAngle(const T& p, double thickness = 5, Color c = Palette::White) {

			if (p.outer().size() == 0) {
				return;
			}

			output(p, thickness, c);

			for (unsigned i = 1; i < p.outer().size(); i++) {
				const auto& v = p.outer()[i];
				font(Puzzle::getInteriorAngle(p, i) / Puzzle::M_PI * 180).draw(Vec2(v.x() * gui_magnification + 5 + gui_move_x, v.y() * gui_magnification + 5 + gui_move_y));
			}
		}

		template<class T>
		//頂点の内角を追加して表示する。
		void outputWithInteriorAngle(const std::vector<T> &vecpie, double thickness = 5, Color c = Palette::White) {
			for (const auto & a : vecpie) {
				outputWithInteriorAngle(a, thickness, c);
			}
		}

		//頂点の外角を追加して表示する。
		template<class T>
		void outputWithExternalAngle(const T p, double thickness = 5, Color c = Palette::White) {

			if (p.outer().size() == 0) {
				return;
			}

			output(p, thickness, c);

			for (unsigned i = 1; i < p.outer().size(); i++) {
				const auto& v = p.outer()[i];
				font(Puzzle::getExternalAngle(p, i) / Puzzle::M_PI * 180).draw(Vec2(v.x() * gui_magnification + 5 + gui_move_x, v.y() * gui_magnification + 5 + gui_move_y));
			}
		}

		template<class T>
		//頂点の外角を追加して表示する。
		void outputWithExternalAngle(std::vector<T> vecpie, double thickness = 5, Color c = Palette::White) {
			for (const auto & a : vecpie) {
				outputWithExternalAngle(a, thickness, c);
			}
		}


		//インナーの頂点の外角を追加して表示する。
		void outputInnersWithExternalAngle(Puzzle::frame f, double thickness = 5, Color c = Palette::White) {

			if (f.inners().size() == 0) {
				return;
			}

			for (unsigned j = 0; j < f.inners().size(); j++) {
				const auto& inner = f.inners()[j];

				for (unsigned i = 0; i < inner.size() - 1; i++) {
					Line(Vec2(inner[i].x() * gui_magnification + gui_move_x, inner[i].y() * gui_magnification + gui_move_y), Vec2(inner[i + 1].x() * gui_magnification + gui_move_x, inner[i + 1].y() * gui_magnification + gui_move_y)).draw(thickness, c);
					font(Puzzle::getExternalAngle(f, j, i) / Puzzle::M_PI * 180).draw(Vec2(inner[i].x() * gui_magnification + 5 + gui_move_x, inner[i].y() * gui_magnification + 5 + gui_move_y));
				}

			}
		}

		//インナーの頂点の内角を追加して表示する。
		void outputInnersWithInteriorAngle(Puzzle::frame f, double thickness = 5, Color c = Palette::White) {

			if (f.inners().size() == 0) {
				return;
			}

			for (unsigned j = 0; j < f.inners().size(); j++) {
				const auto& inner = f.inners()[j];

				for (unsigned i = 0; i < inner.size() - 1; i++) {
					Line(Vec2(inner[i].x() * gui_magnification + gui_move_x, inner[i].y() * gui_magnification + gui_move_y), Vec2(inner[i + 1].x() * gui_magnification + gui_move_x, inner[i + 1].y() * gui_magnification + gui_move_y)).draw(thickness, c);
					font(Puzzle::getInteriorAngle(f, j, i) / Puzzle::M_PI * 180).draw(Vec2(inner[i].x() * gui_magnification + 5 + gui_move_x, inner[i].y() * gui_magnification + 5 + gui_move_y));
				}

			}
		}

		//フレーム用innersの表示
		void outputInners(Puzzle::frame f, double thickness = 5, Color c = Palette::White) {

			if (f.inners().size() == 0) {
				return;
			}

			for (const auto& inner : f.inners()) {
				for (unsigned i = 0; i < inner.size() - 1; i++) {
					Line(Vec2(inner[i].x() * gui_magnification + gui_move_x, inner[i].y() * gui_magnification + gui_move_y), Vec2(inner[i + 1].x() * gui_magnification + gui_move_x, inner[i + 1].y() * gui_magnification + gui_move_y)).draw(thickness, c);
				}
			}
		}

		void outputInnersAndPoint(Puzzle::frame f, double thickness = 5, Color c = Palette::White) {

			if (f.inners().size() == 0) {
				return;
			}

			for (const auto& inner : f.inners()) {
				LineString ls;

				for (const auto& v : inner) {
					ls.push_back(Vec2(v.x() * gui_magnification, v.y() * gui_magnification));
					Circle(Vec2(v.x() * gui_magnification + gui_move_x, v.y() * gui_magnification + gui_move_y), 2).draw(Palette::Palevioletred);
				}

				//たまに中が白くなるバグってるかも？
				ls.asPolygon(thickness, true).draw(Color(c, 125));
			}
		}

		void outputInnersWithNumber(Puzzle::frame f, double thickness = 5, Color c = Palette::White) {

			if (f.inners().size() == 0) {
				return;
			}

			outputInners(f, thickness, c);

			for (auto const & inner : f.inners()) {

				for (unsigned i = 1; i < inner.size(); i++) {
					const auto& v = inner[i];
					font(i).draw(Vec2(v.x() * gui_magnification + 5 + gui_move_x, v.y() * gui_magnification + 5 + gui_move_y));
				}
			}
		}

		//webカメラの画像を表示する。
		bool displayCamera();

		//webカメラの画像を撮影する。
		//撮影したものは保存され画像解析される。
		bool takePicture();

		//webカメラの画像を撮影する。
		//撮影したものは保存され画像解析される。
		int takeInformationPicture();

		//webカメラの画像を撮影する。
		//撮影したものは保存され画像解析され保存される。
		bool takePicture(String file_name);

		//GUI指定による探索用
		//マウスと頂点との距離が5以下なら光らせクリックされていたなら返す。

		std::pair<int, int> selectPoint(Puzzle::frame f, double distance = 5, Color c = Palette::White) {

			if (f.inners().size() == 0) {
				return{ -1,-1 };
			}

			for (unsigned j = 0; j < f.inners().size(); j++) {
				const auto& inner = f.inners()[j];
				for (unsigned i = 0; i < inner.size() - 1; i++) {

					const Vec2 & dis = Vec2(inner[i].x() * gui_magnification + gui_move_x, inner[i].y() * gui_magnification + gui_move_y) - Mouse::Pos();

					if (hypot(dis.x, dis.y) <= distance) {
						Circle(Vec2(inner[i].x() * gui_magnification + gui_move_x, inner[i].y() * gui_magnification + gui_move_y), distance).draw(c);

						if (Input::MouseL.clicked) {
							return{ j,i };
						}

					}

				}
			}

			return{ -1,-1 };
		}

		//GUI指定による探索用
		//マウスと辺の中心との距離が5以下なら光らせクリックされていたなら返す。

		std::pair<int, int> selectEdge(Puzzle::frame f, double distance = 5, Color c = Palette::Yellow) {

			if (f.inners().size() == 0) {
				return{ -1,-1 };
			}

			for (unsigned j = 0; j < f.inners().size(); j++) {
				const auto& inner = f.inners()[j];
				for (unsigned i = 0; i < inner.size() - 1; i++) {

					const Vec2 & dis = (Vec2(inner[i].x() * gui_magnification + gui_move_x, inner[i].y() * gui_magnification + gui_move_y)
						+ Vec2(inner[i + 1].x() * gui_magnification + gui_move_x, inner[i + 1].y() * gui_magnification + gui_move_y)) / 2


						- Mouse::Pos();

					if (hypot(dis.x, dis.y) <= distance) {
						Circle(((Vec2(inner[i].x() * gui_magnification + gui_move_x, inner[i].y() * gui_magnification + gui_move_y)
							+ Vec2(inner[i + 1].x() * gui_magnification + gui_move_x, inner[i + 1].y() * gui_magnification + gui_move_y)) / 2), distance).draw(c);

						if (Input::MouseL.clicked) {
							return{ j,i };
						}

					}

				}
			}

			return{ -1,-1 };
		}

	};

}