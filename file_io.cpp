#include "file_io.h"

//これはそんなコメントすることない公式の入力フォーマット読んで。

//そんなつかわなかった
const std::string Puzzle::file_manager::output_file_name = "output.txt";
//errorを吐き出すファイル名
const std::string Puzzle::file_manager::error_file_name = "error.txt";


void Puzzle::file_manager::output_file(std::string s)
{

	std::ofstream fs(output_file_name, std::ios::app);

	fs << s << std::endl;

	fs.close();

}

void Puzzle::file_manager::output_error(std::string s)
{

	std::ofstream fs(error_file_name, std::ios::app);

	auto time = std::chrono::system_clock::now();

	std::time_t now_time = std::chrono::system_clock::to_time_t(time);

	fs << "[" << std::put_time(std::localtime(&now_time), "%c") << "]" << s << std::endl;

	fs.close();
}

std::vector<Puzzle::piece> Puzzle::file_manager::input_piece(std::string file_name)
{

	std::ifstream file;
	file.open(file_name, std::ios::in);

	std::vector<piece> piece_data;

	if (file.bad()) {
		return piece_data;
	}
	else {

		std::string buffer;

		getline(file, buffer, ':');

		if (buffer == "QR-Code") {

			getline(file, buffer, ':');

			//図形数
			int figures_number = std::stoi(buffer);

			for (int i = 0; i < figures_number; i++) {

				//一時的に頂点データを保存する
				piece vertex_data;

				getline(file, buffer, ' ');
				int vertex_position_x;
				int vertex_position_y;

				//頂点数
				int vertex_number = std::stoi(buffer);

				for (int j = 0; j < vertex_number - 1; j++) {
					getline(file, buffer, ' ');
					vertex_position_x = std::stoi(buffer);

					getline(file, buffer, ' ');
					vertex_position_y = std::stoi(buffer);

					vertex_data.outer().push_back({ vertex_position_x,vertex_position_y });

				}
				getline(file, buffer, ' ');
				vertex_position_x = std::stoi(buffer);
				getline(file, buffer, ':');
				vertex_position_y = std::stoi(buffer);

				vertex_data.outer().push_back({ vertex_position_x, vertex_position_y });

				//取得した図形の頂点データを追加する。
				boost::geometry::correct(vertex_data.polygon);
				piece_data.push_back(vertex_data);
			}


		}
	}
	return piece_data;
}

Puzzle::frame Puzzle::file_manager::input_frame(std::string file_name) {

	std::ifstream file;
	file.open(file_name, std::ios::in);

	frame f;

	if (file.bad()) {
		return f;
	}
	else {

		std::string buffer;

		getline(file, buffer, ':');

		if (buffer == "QR-Code") {

			getline(file, buffer, ':');

			//図形数
			int figures_number = std::stoi(buffer);

			for (int i = 0; i < figures_number; i++) {
				//ピースは読み飛ばす。
				getline(file, buffer, ':');

			}

			for (int k = 0; k < 3; k++) {
				getline(file, buffer, ' ');

				if (buffer.size() == 0 || file.eof()) {
					boost::geometry::correct(f.polygon);
					return f;
				}

				f.inners().push_back(Puzzle::polygon_type::ring_type());

				//ワクの頂点数
				int frame_vertex_number = std::stoi(buffer);

				int vertex_position_x;
				int vertex_position_y;

				for (int j = 0; j < frame_vertex_number - 1; j++) {
					getline(file, buffer, ' ');
					vertex_position_x = std::stoi(buffer);

					getline(file, buffer, ' ');
					vertex_position_y = std::stoi(buffer);

					f.inners()[k].push_back({ vertex_position_x, vertex_position_y });
				}

				getline(file, buffer, ' ');
				vertex_position_x = std::stoi(buffer);

				getline(file, buffer, ':');
				vertex_position_y = std::stoi(buffer);

				f.inners()[k].push_back({ vertex_position_x, vertex_position_y });


			}
		}
	}

	return f;
}

void Puzzle::file_manager::output_figure(const std::vector<Puzzle::polygon_type>& pieces, std::string file_name)
{
	std::ofstream fs(file_name);

	fs << pieces.size();
	for (int i = 0; i < pieces.size(); i++) {
		fs << ":" << pieces[i].outer().size();
		for (int j = 0; j < pieces[i].outer().size(); j++) {
			fs << " " << pieces[i].outer()[j].x() << " " << pieces[i].outer()[j].y();
		}
	}

	fs << std::endl;

	fs.close();

}

void Puzzle::file_manager::output_state(const Puzzle::frame & f, const std::vector<Puzzle::piece> & pieces, std::string file_name)
{

	std::ofstream fs(file_name);

	fs << pieces.size();
	for (int i = 0; i < pieces.size(); i++) {
		fs << ":" << pieces[i].outer().size();
		for (int j = 0; j < pieces[i].outer().size(); j++) {
			fs << " " << pieces[i].outer()[j].x() << " " << pieces[i].outer()[j].y();
		}
	}

	for (int i = 0; i < f.inners().size(); i++) {
		fs << ":" << f.inners()[i].size();
		for (int j = 0; j < pieces[i].outer().size(); j++) {
			fs << " " << f.inners()[i][j].x() << " " << f.inners()[i][j].y();
		}
	}

	fs << std::endl;

	fs.close();

}
