#pragma once

#include "procon2017.h"

namespace Puzzle {

	struct operation {

		//平行移動量
		int moveX;
		int moveY;

		//反転フラグ　falseならそのまま
		bool reverseFlag;
		int reverseX;

		//回転軸
		int rotateX;
		int rotateY;
		//回転量
		double rotateTheta;

		operation() {
			moveX = 0;
			moveY = 0;
			reverseFlag = false;
			reverseX = 0;
			rotateX = 0;
			rotateY = 0;
			rotateTheta = 0;
		}

	};


	class piece {
	public:
		//高速化用の布石 ビットであらわされる。
		unsigned long long int  piece_bit;
		//ピースの番号
		int piece_number;

		polygon_type polygon;
		operation ope;

		bool piece_arrange;

		inline polygon_type::ring_type & outer() {
			return polygon.outer();
		}

		inline polygon_type::ring_type const& outer() const {
			return polygon.outer();
		}

		std::vector<polygon_type> getComposition();

		//問答無用で全部コピーしてる。
		piece(const piece& p) {
			ope = p.ope;
			polygon = p.polygon;
			piece_bit = p.piece_bit;
			piece_number = p.piece_number;

			piece_arrange = p.piece_arrange;

			if (p.A)
				A = p.A;
			if (p.B)
				B = p.B;
		}

		//ポリゴンの領域をケチってる。
		//よくないこういう書き方は非常に良くない。美しくない。誰か書き換えて。
		piece(const piece& p, bool t) {
			ope = p.ope;
			piece_number = p.piece_number;
			piece_bit = p.piece_bit;

			if (t)
				if (!(p.A && p.B))
					polygon = p.polygon;

			piece_arrange = p.piece_arrange;

			if (p.A)
				A = p.A;
			if (p.B)
				B = p.B;
		}

		piece() {
			ope = operation();
			polygon = polygon_type();
			A = B = std::shared_ptr<piece>();
			piece_bit = 0;
			piece_number = -1;
			piece_arrange = false;
		}

		//新しいピースを生成する際のコンストラクタ。
		piece(polygon_type p, std::shared_ptr<piece> a, std::shared_ptr<piece> b);

		bool isSmall();

		//複雑性を評価する関数。
		//辺*100+面積
		int evalComplexity();

	private:
		std::shared_ptr<piece> A;
		std::shared_ptr<piece> B;
	};

	//ピースを反転させる。
	//反転時の動作、二回反転すると同じ位置に添え字、reverseFlagも含めて戻る。
	bool canInvert(piece &A);

	//ポリゴンを反転させる。
	//反転時の動作、二回反転すると同じ位置に添え字も含めて戻る。
	bool canInvert(polygon_type &A, int axis_x);

	//ピースを移動させる。
	bool canMove(piece & A, int vx, int vy);

	//ポリゴンを移動させる。
	bool canMove(polygon_type & A, int vx, int vy);

	//ピースを回転させる。
	//2pi,-2pi回転すると0度になる
	bool canRotate(piece & A);

	//ポリゴンを回転させる。
	bool canRotate(polygon_type& A, int axis_x, int axis_y, double angle);

	//ピースをシンプルにする。（直線近似）
	//直線状に余計な頂点を存在させないようにする。関数。
	//
	// 1--2--3 => 1-----2 とする。
	//
	bool canSimply(piece &A);
	bool canSimply(polygon_type &A);

	//キャスト
	Puzzle::polygon_type_double convertePolygon(Puzzle::polygon_type A);

	//キャスト
	Puzzle::polygon_type convertePolygon(Puzzle::polygon_type_double A);

	//unionシリーズの高速化
	piece unionBySide(piece A, piece B, int va, int vb);
	piece unionByDeg(piece A, piece B, int va, int vb);

	//中心の頂点を与えると内角を返す。
	//内角とは、ピースがある側の角度。
	double getInteriorAngle(const piece& A, const int va);
	double getInteriorAngle(const polygon_type& A, const int va);

	//中心の頂点を与えると外角を返す。
	//外角とは、ピースがない側の角度。
	double getExternalAngle(const piece& A, const int va);
	double getExternalAngle(const polygon_type& A, const int va);

	//高速化用の布石
	//いろいろメモして高速化
	//ピースの情報をメモしておくためのクラス。
	struct piece_aggregation {

		//初期のピースにおいて何番目に位置するかをbitで	
		unsigned long long int number_bit;

		//回転を表す。
		std::unordered_map<double, Puzzle::piece> rotation;

		//反転後の回転を表す。
		std::unordered_map<double, Puzzle::piece> invert_rotation;

		//すべての辺の長さを足したもの
		double total_edge_length;

		//各辺の長さ
		std::vector<double> edge_length;

		//反転後の各辺の長さ
		std::vector<double> invert_edge_length;

		//各頂点ごとの内角
		std::vector<double> vertex_angle;

		//反転後の各頂点の内角
		std::vector<double> invert_vertex_angle;

		piece_aggregation(piece p);

	};


	//シングルトン　リソースを管理する。canRotate呼ぶな!!!!
	class piece_manager {
	private:
		piece_manager() = default;
		~piece_manager() = default;

		//計算済みのデータをいれてる。piece_numberでアクセスして
		std::vector<piece_aggregation> data;

	public:

		piece_manager(const piece_manager&) = delete;
		piece_manager& operator=(const piece_manager&) = delete;
		piece_manager(piece_manager&&) = delete;
		piece_manager& operator=(piece_manager&&) = delete;

		static piece_manager& get_instance();

		//初期化　必ず呼び出すこと
		void initialize(std::vector<Puzzle::piece>& vp);

		//ピースを取得する　使いづらいけどしゃあない
		//piece getPiece(int piece_num, operation ope);
		bool getPiece(int piece_num, operation ope, piece & pi);

		//ポリゴンを取得する変化しないデータにどうぞ
		polygon_type& getPolygon(int piece_num);

		//ポリゴンを取得する変化しないデータにどうぞ
		polygon_type& getPolygonInvert(int piece_num);

		//データにアクセスする。
		Puzzle::piece_aggregation& getData(int piece_num);

		//組み合わせた結果を加える　ピースのビットを渡す
		//void addPiece(piece &pi, int piece_num_r, int piece_num_l);
		//組み合わせた結果を加える　ピースのビットを渡す　そのピースに割り当てられた番号を返す。
		//バグイ　結局は無駄な計算をしないようにすればいいだけだからピース番号で管理して。計算済みのを返すようにした。それだけ。
		int addPiece(polygon_type &poly, unsigned long long int  piece_bit);
		
		//配置情報を使用するために書き換える
		void changePolygon(int piece_num , polygon_type poly);

		//基本ピースの数
		int base_total;
		//組み合わせピースの数 ピース増やしたときいじり忘れるなよ
		int combi_total;
	};

	//同一かいろいろと頑張って判定する
	bool pieceEquals(int piece_num, polygon_type poly);

}