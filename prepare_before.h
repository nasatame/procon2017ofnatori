//range,degreeマップ生成プログラム一応残しておく
#pragma once

#include "procon2017.h"
#include "prepare.h"

namespace Puzzle {

	//K後輩とY後輩のお仕事
	static void create_map_calc() {
		int hypotenuse;
		double degree;
		for (int i = -110; i <= 110; i++) {
			for (int j = -110; j <= 110; j++) {
				if (i == 0) {
					if (j < 0) {
						degree = 1.5 * M_PI;
					}
					else if (j > 0) {
						degree = 0.5 * M_PI;
					}
					else {
						continue;
					}
				}
				else {
					degree = std::atan2(j, i);
					if (degree < 0) {
						degree = 2 * M_PI + degree;
					}
				}
				hypotenuse = i*i + j*j;
				range_map[hypotenuse].push_back({ i,j });
				degree_map[{i, j}] = degree;
			}
		}
		//std::cerr << "create complate!" << std::endl;
	}
}