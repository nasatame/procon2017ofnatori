#pragma once
#include "procon2017.h"
#include "piece.h"
#include "frame.h"

namespace Puzzle {

	struct piece_info {
		int piece_num;
		Puzzle::operation ope;

		//移動可能フラグ
		bool piece_arrange;

		piece_info() {
			piece_num = 0;
			piece_arrange = false;
		}
	};

	class state {
	public:
		//まだ使用していいない組み合わせたピースたち。
		std::vector<piece_info> pieces;
		//現在のパズルの状況
		Puzzle::frame main_frame;
		//評価値
		int score;

		//次のstateの状態を列挙する関数。
		std::vector<state> getNextState();

		//次のstateの状態を列挙する関数。こぴっただけ、案外高速化に効果ある。
		std::vector<state> getNextStateDeg();
		//次のstateの状態を列挙する関数。こぴっただけ、案外高速化に効果ある。
		std::vector<state> getNextStateSide();

		//辺のみで行う＋整数は入れない。強者候補。
		std::vector<Puzzle::state> getNextStateSidePlus();

		//いろいろ組み合わせる
		//評価を行いながらやる
		std::vector<Puzzle::state> getNextStateEval();

		//getNextState改変版
		//getNextStateOnlySide
		//一つしかの完全に合致する物がない時にしか入れない。
		//getNextStateOnlyDeg
		//一つしか完全に合致するものがない時にしか入れない。
		//という方針に基づくgetNextState
		//pck忙しい　先輩作って

		std::vector<Puzzle::state> getNextStateOnly();

		//小さいのを除いて
		std::vector<Puzzle::state> getNextStateNotSmall();

		//角度のみで行う＋90度関係は入れない。多分一番強い。
		std::vector<Puzzle::state> getNextStateDegPlus();

		//次のstateの状態を列挙する関数。頑張って高速化、大して変わらんけど
		void getNextState(std::vector<state>& states, std::unordered_map<unsigned long long int, std::vector<unsigned>>& hash);
		

		//GUIを用いた探索補助用の関数。
		std::vector<Puzzle::state> Puzzle::state::getNextStateSelectPoint(int fnum, int pnum);
		//GUIを用いた探索補助用の関数。
		std::vector<Puzzle::state> Puzzle::state::getNextStateSelectEdge(int fnum, int pnum);

		//評価関数チーム用
		//一番いいのを頼む
		int evaluateKinnNiku();

		state() {
			score = 0;
		}

		state(Puzzle::frame m, std::vector<piece_info> p) : pieces(p), main_frame(m) {
			score = 0;
		}

		state(Puzzle::frame m, std::vector<piece> p) : main_frame(m) {
			score = 0;
			piece_info pi;
			for (int i = 0; i < p.size(); i++) {
				pi.piece_num = p[i].piece_number;
				pi.piece_arrange = p[i].piece_arrange;
				pieces.push_back(pi);
			}
		}

		state(Puzzle::frame m, std::vector<piece_info> p,int s) : pieces(p), main_frame(m) , score(s) {

		}

		//後輩のお仕事
		//返り値は自由に決めてくれて結構です。
		//Hashの精度と比較時間はトレードオフです。精度は十分でてるみたい
		//main_frame.inners().size() >= 1
		unsigned long long int getHash() const;

		//後輩のお仕事
		//このフレームから解答につながる可能性があるか返す関数。
		//ある場合は,true、ない場合は,false
		bool isAnswerPossibility();

		//
		int minPieceArea();
		int minInnerArea();

		//全てのフレームの辺に対応する辺があるか。を判定したいが容易に無理そうなので。最小の辺以下の辺があるかだけ。
		bool equalEdge();
		//同じ理由で容易に無理そうなので。90度以下のみ
		bool equalDegree();

		//一番小さい辺を返すシリーズ
		double minPieceEdge();
		double minInnerEdge();

		//一番小さい角度を返すシリーズ
		double minPieceAngle();
		double minInnerAngle();


	private:
	};

	bool operator<(const state& a, const state& b);

	bool operator==(const state& a, const state& b);

	class beam_search {
	public:
		
		static const int beam_width = 100;

		//正式版成績がよかったものを使用する。
		state solve(frame f,std::vector<piece_info> pieces);


		//変則GUIビームサーチ
		//別スレッドに分割し並列して計算処理を行う。
		//列挙関数と評価関数を指定できる。
		static int solveGuiSuzukiFunc(std::vector<Puzzle::state>& nowState, std::vector<Puzzle::state>& nextState,int funcEnum,int funcEval,
			Puzzle::state& answer, std::mutex& mtx, int & progressShare);

		//chokudai サーチ
		static int chokudaiSearchBySuzuki(std::vector<Puzzle::state>& nowState, std::vector<Puzzle::state>& nextState, int funcEnum, int funcEval,
			Puzzle::state& answer, std::mutex& mtx, int & progressShare);

		//全探索 ラスト１０をつめるときに使う。
		static int fullSearchBySuzuki(std::vector<Puzzle::state>& nowState, std::vector<Puzzle::state>& nextState, int funcEnum, int funcEval,
			Puzzle::state& answer, std::mutex& mtx, int & progressShare);

		//Beam Stack Search 序盤から終盤まで全力でぶん殴る
		static int beamStackSearchBySuzuki(std::vector<Puzzle::state>& nowState, std::vector<Puzzle::state>& nextState, int funcEnum, int funcEval,
			Puzzle::state& answer, std::mutex& mtx, int & progressShare);

		//焼きなまし法//構想のみ
		//static int

	private:



	};

	//後輩のお仕事
	//引数に対して除外を実行してください。
	int excludeState(std::vector<Puzzle::state> & states);



	//そのうち欲しい　そのフレームの内側にはまるか。
	bool isFrameIn(Puzzle::frame f,Puzzle::piece p);


	//効率的なunionBySide。最終的にこっちしか使ってない
	bool unionBySide(Puzzle::frame f,Puzzle::piece_info pi, int i0, int i1, int i2,frame & result);

	//効率的なunionByDeg。最終的にこっちしか使ってない
	bool unionByDeg(Puzzle::frame f, Puzzle::piece_info pi, int i0, int i1, int i2, frame & result);
	
}