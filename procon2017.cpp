#include "procon2017.h"

//よく使う処理の集合

size_t Puzzle::correctSuffix(int i, size_t size)
{
	if (i >= 0 && i < size)return i;
	if (i < 0)return size + i - 1;
	if (i >= size)return i - size + 1;
	return 0;
}

//abs(a/b - c/d) < ε
bool Puzzle::divEqual(int a, int b, int c, int d)
{
	/*
	if (a*a*d*d - 2 * a*b*c*d + c*c*b*b == 0 ^ abs(a*c + b*d - sqrt(a*a + b*b)*sqrt(c*c + d*d)) < 0.000000000001) {
		assert(false);
	}*/

	c *= -1;
	d *= -1;

	if (abs(a*c + b*d - sqrt(a*a + b*b)*sqrt(c*c + d*d)) < 0.000000000001) {
		return true;
	}
	else {
		return false;
	}
}

double Puzzle::floorTen(double a)
{
	return floor(10000000000 * a) / 10000000000;
}

/*
distance(a,b) == distance(c,d)
*/
bool Puzzle::equalDistance(Puzzle::polygon_type::point_type a, Puzzle::polygon_type::point_type b, Puzzle::polygon_type::point_type c, Puzzle::polygon_type::point_type d)
{
	if ((a.x() - b.x())*(a.x() - b.x()) + (a.y() - b.y())*(a.y() - b.y()) ==
		(c.x() - d.x())*(c.x() - d.x()) + (c.y() - d.y())*(c.y() - d.y())) {
		return true;
	}
	else {
		false;
	}
}
