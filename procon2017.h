#pragma once

#include <iostream>
#include <vector>
#include <algorithm>
#include <array>
#include <functional>
#include <queue>
#include <cmath>
#include <cstdlib>
//boost入れてね
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/assign/list_of.hpp>
#include <memory>
#include <future>
#include <unordered_map>
#include <thread>
#include <functional>
#include <map>


namespace Puzzle{
	const double M_PI = 3.14159265358979323846;
	typedef boost::geometry::model::polygon< boost::geometry::model::d2::point_xy<int> > polygon_type;
	typedef boost::geometry::model::polygon< boost::geometry::model::d2::point_xy<double> > polygon_type_double;

	//添え字。何とかしますよ。てきなやつ。
	//iはsizeの2倍以上のあれが来ることは想定していない
	size_t correctSuffix(int i, size_t size);

	//abs(a/b - c/d) < ε
	bool divEqual(int a, int b, int c, int d);

	//小数点以下１０桁までで切り捨てる関数。
	double floorTen(double a);

	//bitをintに
	//未使用



	//intをbitに
	//未使用

	/*
	distance(a,b) == distance(c,d)
	*/
	bool equalDistance(Puzzle::polygon_type::point_type a,
		Puzzle::polygon_type::point_type b,
		Puzzle::polygon_type::point_type c,
		Puzzle::polygon_type::point_type d);

}