#include "piece.h"
#include "file_io.h"

//一番初期の元気があったころに書いたコードまとも
//ここが終わったあたりから辛くなってコードが汚くなったり命名規則が乱れたりし始めた。

const double M_PI = 3.14159265358979323846;

bool Puzzle::canInvert(piece & A) {

	int t = A.outer()[0].x();

	//２度反転すると元の位置に戻る
	if (A.ope.reverseFlag) {
		A.ope.reverseFlag = false;

		return canInvert(A.polygon, A.ope.reverseX);
	}

	for (auto & v : A.outer()) {
		v.set<0>(v.x() * -1 + 2 * t);
	}

	boost::geometry::correct(A.polygon);

	A.ope.reverseFlag = true;
	A.ope.reverseX = t;

	return true;
}

bool Puzzle::canInvert(polygon_type & A,int axis_x)
{
	int t = axis_x;

	for (auto & v : A.outer()) {
		v.set<0>(v.x() * -1 + 2 * t);
	}

	boost::geometry::correct(A);

	return false;
}

bool Puzzle::canMove(polygon_type & A, int vx, int vy)
{
	for (auto & v : A.outer()) {
		v.set<0>(v.x() + vx);
		v.set<1>(v.y() + vy);
	}

	return true;
}

bool Puzzle::canMove(piece & A, int vx, int vy)
{
	for (auto & v : A.outer()) {
		v.set<0>(v.x() + vx);
		v.set<1>(v.y() + vy);
	}

	A.ope.moveX = vx;
	A.ope.moveY = vy;

	return true;
}

//ばぐってる可能性アリ
//デバッグ求む。
bool Puzzle::canSimply(piece & A)
{
	polygon_type B;

	//B.outer().push_back(A.outer()[0]);

	auto v0 = A.outer()[A.outer().size()-2];
	auto v1 = A.outer()[0];
	auto v2 = A.outer()[1];

	auto isCollinear = [&]() {
		if ((v1.y() - v0.y())*(v2.x() - v0.x()) == (v2.y() - v0.y())*(v1.x() - v0.x())) {
			return true;
		}
		else {
			return false;
		}
	};

	if (isCollinear()) {

	}
	else {
		B.outer().push_back(v1);
	}

	for (unsigned i = 2; i < A.outer().size(); i++) {
		v0 = A.outer()[i-2];
		v1 = A.outer()[i-1];
		v2 = A.outer()[i];

		if (isCollinear()) {

		}
		else {
			B.outer().push_back(v1);
		}

	}

	B.outer().push_back(B.outer()[0]);

	A.polygon = B;

	return true;
}

bool Puzzle::canSimply(polygon_type & A)
{
	polygon_type B;

	//B.outer().push_back(A.outer()[0]);

	auto v0 = A.outer()[A.outer().size() - 2];
	auto v1 = A.outer()[0];
	auto v2 = A.outer()[1];

	auto isCollinear = [&]() {
		if ((v1.y() - v0.y())*(v2.x() - v0.x()) == (v2.y() - v0.y())*(v1.x() - v0.x())) {
			return true;
		}
		else {
			return false;
		}
	};

	if (isCollinear()) {

	}
	else {
		B.outer().push_back(v1);
	}

	for (unsigned i = 2; i < A.outer().size(); i++) {
		v0 = A.outer()[i - 2];
		v1 = A.outer()[i - 1];
		v2 = A.outer()[i];

		if (isCollinear()) {

		}
		else {
			B.outer().push_back(v1);
		}

	}

	B.outer().push_back(B.outer()[0]);

	A = B;

	return true;
}

//仕様追加、合成の結果ピースの内側に空白が出来たらはじくようにする。
Puzzle::piece Puzzle::unionBySide(piece A, piece B, int va, int vb)
{
	/*
		A:

		1--3
		! /
		!/
		2

		B:

		1--3
		! /
		!/
		2

		va:1
		vb:1

		この場合Aの1-2とBの1-2をくっつけるということになる。そうすると、

		3--1--3
		 \ ! /
		  \!/
		   2

		という解釈でいい？でも

		   1--3
		  /! /
		 / !/
		3--2
		もありえる。現在の引数では反転するしないで、分ける情報を与えられないので反転に関しては外部依存でもいいかな？
		operatorに情報を追加するのは各関数内でやったほうがいいかも。

		現在反転は使用しないこととする。
	*/

	//もしあれば、
	//反転する必要があるか計算する。

	//反転
	//ここまで

	//回転
	//接合するその辺の角度を割り出して上手くくっつくようにAorBを回転させる

	std::pair<int, int> target(A.outer()[va].y() - A.outer()[va + 1].y(), A.outer()[va].x() - A.outer()[va + 1].x());

	while (target != std::pair<int, int>(B.outer()[vb + 1].y() - B.outer()[vb].y(), B.outer()[vb + 1].x() - B.outer()[vb].x())) {
		canRotate(B);
		if (B.ope.rotateTheta == 0) return piece();
	}

	//移動
	//範囲外に出たらやばいので、Aの位置をずらして常に中心に来るように移動する。
	canMove(A, -1 * A.outer()[0].x(), -1 * A.outer()[0].y());

	//最終的にA(1-2),B(1-2)が上手くくっつくように移動する。

	canMove(B, A.outer()[va + 1].x() - B.outer()[vb].x(), A.outer()[va + 1].y() - B.outer()[vb].y());

	//エラー制御とりあえず面積を見て足されたか確認すればOKかと思われる。

	std::vector<polygon_type_double> vec_poly;
	polygon_type_double Apoly = convertePolygon(A.polygon);
	polygon_type_double Bpoly = convertePolygon(B.polygon);

	boost::geometry::union_(Apoly, Bpoly, vec_poly);

	//図形数からエラーを検出する。
	if (vec_poly.size() == 1 && vec_poly[0].inners().size() == 0) {

	}
	else {
		return piece();
	}

	//面積的な面からエラーを検出する。
	if (std::abs(boost::geometry::area(A.polygon) + boost::geometry::area(B.polygon) - boost::geometry::area(vec_poly[0]))<0.0000000001) {

	}
	else {
		return piece();
	}

	boost::geometry::correct(vec_poly[0]);

	//返すピースのA,Bのshared_ptrに親となった二つのピースを登録する。

	piece result(convertePolygon(vec_poly[0]), std::shared_ptr<piece>(new piece(A, true)), std::shared_ptr<piece>(new piece(B, true)));
	
	canSimply(result);

	return result;
}

//仕様追加、合成の結果ピースの内側に空白が出来たらはじくようにする。
Puzzle::piece Puzzle::unionByDeg(piece A, piece B, int va, int vb)
{
	/*
	
	A : va == 4

	.
	.
	.
	3
	|
	|
	4--------5...

	B : vb == 1

	0
	|\
	| \
	1--2
	
	ある角度開いているのと閉じているのが見つかったら。（この開いているのと閉じているのには去年の知識を使う）

	まず、回転する。canRotate(piece&)
	次に移動する。canMove(piece&);

	この順番は絶対に守らなきゃいけない。復元時のため。

	詳細
	回転時。
	その開いている角度を構成している右の辺の角度と、左の辺の角度を取得する。
				
				va+1
			 右
			 /
va-1		/
	左--中心

	(vb-1)の辺と、(va-1)の辺のx軸から見た角度が等しい。

	左:180度
	右:60度

	まず、Bを回転させていき。A,Bともに両辺が同じ角度になるか試してみる。
	1回転したらB.ope.rotateThetaが0になるから。

	while(aが一回転){
		aの回転
		while(bが一回転){
			b回転
		}
	}

	それを見て、次にA少しを回転させる。（Aがそのままでははまらないパターンが考えられなくもない）
	その次、Bをまた１回転する。

	これをAが１周するまで繰り返す。

	もし、両辺が同じ角度になったら。

	移動時。

	中心同士、（va,vb）をあわせる。
	このときva,vbそれぞれを0,0にあわせると処理が簡略化できるのでは。（好みの問題）

	接合時。

	unionは自己交差を含んだ図形しか出来ないとout.size() == 0になるので、サイズのチェックを先に行う。

	//図形数からエラーを検出する。
	if (vec_poly.size() == 1) {

	}
	else {
	return piece();
	}
	//面積的な面からエラーを検出する。
	if (std::abs(boost::geometry::area(A.polygon) + boost::geometry::area(B.polygon) - boost::geometry::area(vec_poly[0])) < 0.00000001) {

	}
	else {
	return piece();
	}

	終了処理。

	//返すピースのA,Bのshared_ptrに親となった二つのピースを登録する。

	piece result(vec_poly[0], std::shared_ptr<piece>(new piece(A, true)), std::shared_ptr<piece>(new piece(B, true)));

	canSimply(result);//直線化する。

	＊＝＝を使うときは小数的に誤差が発生しないか考える。発生する際はstd::abs(a-b) < 0.000000001 

	*/

/*
	double slopeA = (A.outer()[correctSuffix(va - 1, A.outer().size())].y() - A.outer()[correctSuffix(va, A.outer().size())].y()) /
		(A.outer()[correctSuffix(va - 1, A.outer().size())].x() - A.outer()[correctSuffix(va, A.outer().size())].x());

	double slopeB = (B.outer()[correctSuffix(vb, B.outer().size())].y() - B.outer()[correctSuffix(vb + 1, B.outer().size())].y()) /
		(B.outer()[correctSuffix(vb, B.outer().size())].x() - B.outer()[correctSuffix(vb + 1, B.outer().size())].x());

	while (abs(slopeA - slopeB) > 0.0000000001) {
		canRotate(A);
		slopeA = (A.outer()[correctSuffix(va - 1, A.outer().size())].y() - A.outer()[correctSuffix(va, A.outer().size())].y()) /
			(A.outer()[correctSuffix(va - 1, A.outer().size())].x() - A.outer()[correctSuffix(va, A.outer().size())].x());
		if (A.ope.rotateTheta == 0) {
			while (abs(slopeA - slopeB) > 0.0000000001) {
				canRotate(B);
				slopeB = (B.outer()[correctSuffix(vb, B.outer().size())].y() - B.outer()[correctSuffix(vb + 1, B.outer().size())].y()) /
					(B.outer()[correctSuffix(vb, B.outer().size())].x() - B.outer()[correctSuffix(vb + 1, B.outer().size())].x());
				if (B.ope.rotateTheta == 0)return piece();
			}
		}
	}*/

	while (!divEqual((A.outer()[correctSuffix(va - 1, A.outer().size())].y() - A.outer()[correctSuffix(va, A.outer().size())].y()),
					(A.outer()[correctSuffix(va - 1, A.outer().size())].x() - A.outer()[correctSuffix(va, A.outer().size())].x()),
					(B.outer()[correctSuffix(vb, B.outer().size())].y() - B.outer()[correctSuffix(vb + 1, B.outer().size())].y()),
					(B.outer()[correctSuffix(vb, B.outer().size())].x() - B.outer()[correctSuffix(vb + 1, B.outer().size())].x()))) {

		canRotate(A);
		if (A.ope.rotateTheta == 0) {
			while (!divEqual((A.outer()[correctSuffix(va - 1, A.outer().size())].y() - A.outer()[correctSuffix(va, A.outer().size())].y()),
							(A.outer()[correctSuffix(va - 1, A.outer().size())].x() - A.outer()[correctSuffix(va, A.outer().size())].x()),
							(B.outer()[correctSuffix(vb, B.outer().size())].y() - B.outer()[correctSuffix(vb + 1, B.outer().size())].y()),
							(B.outer()[correctSuffix(vb, B.outer().size())].x() - B.outer()[correctSuffix(vb + 1, B.outer().size())].x()))) {

				canRotate(B);
				if (B.ope.rotateTheta == 0)return piece();
			}
		}
	}

	//こっからコピペだから俺は悪くない
	canMove(A, -1 * A.outer()[va].x(), -1 * A.outer()[va].y());


	canMove(B, -1 * B.outer()[vb].x(), -1 * B.outer()[vb].y());


	std::vector<polygon_type_double> vec_poly;

	polygon_type_double Apoly = convertePolygon(A.polygon);
	polygon_type_double Bpoly = convertePolygon(B.polygon);


	boost::geometry::union_(Apoly, Bpoly, vec_poly);

	if (vec_poly.size() == 1 && vec_poly[0].inners().size() == 0) {

	}

	else {
		return piece();
	}


	if (std::abs(boost::geometry::area(A.polygon) + boost::geometry::area(B.polygon) - boost::geometry::area(vec_poly[0]))<0.0000000001) {

	}

	else {
		return piece();
	}


	boost::geometry::correct(vec_poly[0]);


	piece result(convertePolygon(vec_poly[0]), std::shared_ptr<piece>(new piece(A, true)), std::shared_ptr<piece>(new piece(B, true)));


	canSimply(result);

	return result;
}

//内角を返す。
double Puzzle::getInteriorAngle(const piece & A,const int va)
{
	//atan2で殴る。外積で殴るより簡単。

	polygon_type::point_type vectorA = A.outer()[correctSuffix(va - 1, A.outer().size())];
	boost::geometry::subtract_point(vectorA, A.outer()[correctSuffix(va, A.outer().size())]);
	polygon_type::point_type vectorB = A.outer()[correctSuffix(va + 1, A.outer().size())];
	boost::geometry::subtract_point(vectorB, A.outer()[correctSuffix(va, A.outer().size())]);

	double result = atan2(vectorB.y(), vectorB.x()) - atan2(vectorA.y(), vectorA.x());

	if (result < 0) {
		result += M_PI * 2;
	}
	else if (result >= M_PI * 2) {
		result -= M_PI * 2;
	}

	return result;
}

double Puzzle::getInteriorAngle(const polygon_type & A, const int va)
{
	//atan2で殴る。外積で殴るより簡単。

	polygon_type::point_type vectorA = A.outer()[correctSuffix(va - 1, A.outer().size())];
	boost::geometry::subtract_point(vectorA, A.outer()[correctSuffix(va, A.outer().size())]);
	polygon_type::point_type vectorB = A.outer()[correctSuffix(va + 1, A.outer().size())];
	boost::geometry::subtract_point(vectorB, A.outer()[correctSuffix(va, A.outer().size())]);

	double result = atan2(vectorB.y(), vectorB.x()) - atan2(vectorA.y(), vectorA.x());

	if (result < 0) {
		result += M_PI * 2;
	}
	else if (result >= M_PI * 2) {
		result -= M_PI * 2;
	}

	return result;
}

//外角を返す。
double Puzzle::getExternalAngle(const piece & A,const int va)
{
	//atan2で殴る。外積で殴るより簡単。

	polygon_type::point_type vectorA = A.outer()[correctSuffix(va - 1, A.outer().size())];
	boost::geometry::subtract_point(vectorA, A.outer()[correctSuffix(va, A.outer().size())]);
	polygon_type::point_type vectorB = A.outer()[correctSuffix(va + 1, A.outer().size())];
	boost::geometry::subtract_point(vectorB, A.outer()[correctSuffix(va, A.outer().size())]);

	double result = atan2(vectorA.y(), vectorA.x()) - atan2(vectorB.y(), vectorB.x());

	if (result < 0) {
		result += M_PI * 2;
	}
	else if (result >= M_PI * 2) {
		result -= M_PI * 2;
	}

	return result;
}

double Puzzle::getExternalAngle(const polygon_type & A, const int va)
{
	//atan2で殴る。外積で殴るより簡単。

	polygon_type::point_type vectorA = A.outer()[correctSuffix(va - 1, A.outer().size())];
	boost::geometry::subtract_point(vectorA, A.outer()[correctSuffix(va, A.outer().size())]);
	polygon_type::point_type vectorB = A.outer()[correctSuffix(va + 1, A.outer().size())];
	boost::geometry::subtract_point(vectorB, A.outer()[correctSuffix(va, A.outer().size())]);

	double result = atan2(vectorA.y(), vectorA.x()) - atan2(vectorB.y(), vectorB.x());

	if (result < 0) {
		result += M_PI * 2;
	}
	else if (result >= M_PI * 2) {
		result -= M_PI * 2;
	}

	return result;
}

bool Puzzle::pieceEquals(int piece_num, polygon_type poly)
{

	std::pair<int, int> min_point_poly = {1000,1000};

	for (int i = 0; i < poly.outer().size(); i++) {
		min_point_poly = std::min(std::make_pair( poly.outer()[i].x(),poly.outer()[i].y()),min_point_poly);
	}

	for (auto & j : piece_manager::get_instance().getData(piece_num).rotation) {

		std::pair<int, int> min_point_piece = { 1000,1000 };

		for (int i = 0; i < j.second.outer().size(); i++) {
			min_point_piece = std::min(std::make_pair(j.second.outer()[i].x(), j.second.outer()[i].y()), min_point_piece);
		}


		for (int i = 0; i < poly.outer().size(); i++) {
			poly.outer()[i].set<0>( poly.outer()[i].x() + min_point_piece.first - min_point_poly.first);
			poly.outer()[i].set<1>(poly.outer()[i].y() + min_point_piece.second - min_point_poly.second);
		}

		if (boost::geometry::equals(j.second.polygon, poly)) {
			return true;
		}

		min_point_poly.first += min_point_piece.first - min_point_poly.first;
		min_point_poly.second += min_point_piece.second - min_point_poly.second;


	}


	for (auto & j : piece_manager::get_instance().getData(piece_num).invert_rotation) {

		std::pair<int, int> min_point_piece = { 1000,1000 };

		for (int i = 0; i < j.second.outer().size(); i++) {
			min_point_piece = std::min(std::make_pair(j.second.outer()[i].x(), j.second.outer()[i].y()), min_point_piece);
		}


		for (int i = 0; i < poly.outer().size(); i++) {
			poly.outer()[i].set<0>(poly.outer()[i].x() + min_point_piece.first - min_point_poly.first);
			poly.outer()[i].set<1>(poly.outer()[i].y() + min_point_piece.second - min_point_poly.second);
		}

		if (boost::geometry::equals(j.second.polygon, poly)) {
			return true;
		}

		min_point_poly.first += min_point_piece.first - min_point_poly.first;
		min_point_poly.second += min_point_piece.second - min_point_poly.second;
	}

	return false;
}

std::vector<Puzzle::polygon_type> Puzzle::piece::getComposition()
{

	if (A.use_count() == 0 || B.use_count() == 0) {
		
		//最下層においてはpolygonをそのまま返せばいいと思われる。

		return std::vector<Puzzle::polygon_type>(1,polygon);
	}

	//A,BのgetCompositionを呼び出す。
	std::vector<Puzzle::polygon_type> vecA = (*A).getComposition();
	std::vector<Puzzle::polygon_type> vecB = (*B).getComposition();

	std::copy(vecB.begin(), vecB.end(), std::back_inserter(vecA));

	for (auto & t : vecA) {
		for (auto & v : t.outer()) {
			file_manager::output_file(std::to_string(v.x()) + " " + std::to_string(v.y()));
		}
		file_manager::output_file("");
	}
	file_manager::output_file("\n");

	//A,Bから渡されたポリゴンそれぞれに現在のピースが行った動作を適応する。

	//反転
	if (ope.reverseFlag) {
		for (auto & t : vecA) {
			canInvert(t,ope.reverseX);
		}
	}

	//回転
	if (ope.rotateTheta != 0) {
		for (auto & t : vecA) {
			if (canRotate(t, ope.rotateX, ope.rotateY, ope.rotateTheta)) {

			}
			else {
				Puzzle::file_manager::output_error("getComposition cann't rotate");
			}
		}
	}

	//移動
	if (ope.moveX != 0 || ope.moveY != 0) {
		for (auto & t : vecA) {
			if (canMove(t, ope.moveX, ope.moveY)) {

			}
			else {
				Puzzle::file_manager::output_error("getComposition cann't move");
			}
		}
	}

	return vecA;
}

Puzzle::piece::piece(polygon_type p, std::shared_ptr<piece> a, std::shared_ptr<piece> b)
{
	{
		polygon = p;

		piece_bit = 0;
		piece_number = -1;

		if (a)
			A = a;
		if (b)
			B = b;

		if (a && b) {
			//ここマジでバグる
			piece_bit = a->piece_bit | b->piece_bit;
			this->piece_number = piece_manager::get_instance().addPiece(p, piece_bit);
		}


	}
}

bool Puzzle::piece::isSmall()
{
	if (polygon.outer().size() <= 10 && boost::geometry::area(polygon) <= 16*9) return true;

	return false;
}

Puzzle::piece_manager & Puzzle::piece_manager::get_instance()
{
	static piece_manager inst;
	return inst;
}

void Puzzle::piece_manager::initialize(std::vector<piece>& vp)
{
	int i = 0;
	for (auto& p : vp) {
		canSimply(p);

		p.piece_bit = (1 << i);
		p.piece_number = i;
		vp[i].piece_number = i;

		data.push_back(piece_aggregation(p));
		data[i].number_bit = (1 << i);

		i++;
	}
	
	base_total = vp.size();
}

Puzzle::polygon_type& Puzzle::piece_manager::getPolygon(int piece_num)
{
	return data[piece_num].rotation[0].polygon;
}

Puzzle::polygon_type & Puzzle::piece_manager::getPolygonInvert(int piece_num)
{
	return data[piece_num].invert_rotation[0].polygon;
}

bool Puzzle::piece_manager::getPiece(int piece_num, operation ope, piece & pi)
{
	if (ope.reverseFlag) {
		pi = data[piece_num].invert_rotation[floorTen(ope.rotateTheta)];
	}
	else {
		pi = data[piece_num].rotation[floorTen(ope.rotateTheta)];
	}
		

	return true;
}

Puzzle::piece_aggregation& Puzzle::piece_manager::getData(int piece_num)
{
	return data[piece_num];
}

int Puzzle::piece_manager::addPiece(polygon_type & poly, unsigned long long int  piece_bit)
{
	
	piece p;
	p.polygon = poly;

	canSimply(p);

	data.push_back(piece_aggregation(p));

	combi_total++;
	return data.size() - 1;
}

void Puzzle::piece_manager::changePolygon(int piece_num , polygon_type poly)
{

	data[piece_num].rotation[0].polygon = poly;

}

Puzzle::piece_aggregation::piece_aggregation(piece p){
	//To北島
	while (true) {
		canRotate(p);
		rotation[p.ope.rotateTheta] = p;
		if (p.ope.rotateTheta == 0)break;
	}

	//invert rotation
	canInvert(p);
	while (true) {
		canRotate(p);
		invert_rotation[p.ope.rotateTheta] = p;
		if (p.ope.rotateTheta == 0)break;
	}

	//total edgelength
	total_edge_length = 0;
	edge_length.resize(p.polygon.outer().size() - 1);
	for (int i = 0; i < p.polygon.outer().size() - 1; i++) {
		edge_length[i] = boost::geometry::distance(p.polygon.outer()[i], p.polygon.outer()[i + 1]);
		total_edge_length += edge_length[i];
	}

}