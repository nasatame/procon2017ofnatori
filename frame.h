#pragma once

#include "procon2017.h"
#include "piece.h"

namespace Puzzle {

	/*
	Caution:frame is fixed
	so frame must not invert , rotate and move.
	*/
	class frame {
	public:

		polygon_type polygon;

		polygon_type::inner_container_type& inners() {
			return polygon.inners();
		}

		inline polygon_type::inner_container_type const& inners() const {
			return polygon.inners();
		}

		inline polygon_type::ring_type & outer() {
			return polygon.outer();
		}

		inline polygon_type::ring_type const& outer() const {
			return polygon.outer();
		}

		//表示されるのがうざいのでフレームの外枠は返さないようにします。
		std::vector<polygon_type> getComposition();

		//問答無用で全部コピーしてる。
		frame(const frame& f) {

			polygon = f.polygon;

			if (f.A)
				A = f.A;
			if (f.B)
				B = f.B;
		}

		//ポリゴンの領域をケチってる。
		//よくないこういう書き方は非常に良くない。美しくない。誰か書き換えて。
		frame(const frame& p, bool t) {

			if (t)
				if (!(p.A && p.B))
					polygon = p.polygon;

			if (p.A)
				A = p.A;
			if (p.B)
				B = p.B;
		}

		frame() {

			polygon = Puzzle::polygon_type();
			A = std::shared_ptr<frame>();
			B = std::shared_ptr<piece>();

			//frame外枠の初期化
			polygon.outer().push_back({ -1,-1 });
			polygon.outer().push_back({ -1,105 });
			polygon.outer().push_back({ 105,105 });
			polygon.outer().push_back({ 105,-1 });
			polygon.outer().push_back({ -1,-1 });
		}

		frame(Puzzle::polygon_type p, std::shared_ptr<frame> a, std::shared_ptr<piece> b) {
			polygon = p;
			if (a)
				A = a;
			if (a)
				B = b;
		}

		//やばい関数を追加
		//直前に追加されたピースの周囲長を返す。
		//これを２スレッドからあつかったりしてるからばぐるんだよ。ばかー
		double pre_perimeter() {
			if (B) {
				return piece_manager::get_instance().getData(B->piece_number).total_edge_length;
			}
			return 0;
		}

		//普通に内側の周囲長を返す関数。
		double perimeter() {
			double length = 0;
			for (const auto & i : polygon.inners()) {

				for (int j = 0; j < i.size()-1; j++) {
					length += boost::geometry::distance(i[j] , i[j + 1]);
				}

			}

			return length;
		}

		int getNewPieceEdgePercent(frame& f);

		double intersectsByKitajima(boost::geometry::model::linestring<boost::geometry::model::d2::point_xy<int>> A, boost::geometry::model::linestring<boost::geometry::model::d2::point_xy<int>> B);

	private:
		std::shared_ptr<Puzzle::frame> A;
		std::shared_ptr<Puzzle::piece> B;
	};


	//フレームをシンプルにする。（直線近似）
	//直線状に余計な頂点を存在させないようにする。関数。
	//
	// 1--2--3 => 1-----2 とする。
	//
	bool canSimply(frame &A);


	//同じ辺の長さを持つピースとフレームを合成する関数。
	Puzzle::frame unionBySide(frame A, piece B, int inum, int va, int vb);

	//同じ辺の長さを持つピースとフレームを合成する関数。
	//合成した結果、完成したときエラー発生時と見分けがつかないためオーバーロードを準備。
	//pieceの方は現状でも十分なためそちらはこのまま使う予定。
	bool unionBySide(frame A, piece B, int inum, int va, int vb,frame & result);

	//角度をあわせると３６０度になる頂点対を持つピースとフレームを合成する関数。
	//返り値はframe
	Puzzle::frame unionByDeg(frame A, piece B, int inum, int va, int vb);

	//角度をあわせると３６０度になる頂点対を持つピースとフレームを合成する関数。
	bool unionByDeg(frame A, piece B, int inum, int va, int vb, frame & result);

	//角度をあわせると３６０度になる頂点対を持つピースとフレームを合成する関数。
	//返り値はframe
	Puzzle::frame unionByDeg(frame A, piece B, int inum, int va, int vb);

	//角度をあわせると３６０度になる頂点対を持つピースとフレームを合成する関数。
	bool unionByDeg(frame A, piece B, int inum, int va, int vb, frame & result);

	//中心の頂点を与えると内角を返す。
	//内角とは、ピースがある側の角度。
	double getInteriorAngle(const frame & A, int inum, const int va);

	//中心の頂点を与えると外角を返す。
	//外角とは、ピースがない側の角度。
	double getExternalAngle(const frame & A, int inum, const int va);

	//移動させないでそのままUnionするシリーズ。
	//返り値が、
	//0ならOK
	//1ならそもそもくっつかない
	//2なら面積的エラー
	int unionByArrange(frame A, piece B, frame &result);

	int unionByArrange(frame A, polygon_type B, frame &result);




}