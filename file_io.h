#pragma once
#pragma warning(disable:4996)

#include <fstream>
#include <string>
#include <chrono>
#include <ctime>
#include <iomanip>
#include "piece.h"
#include "frame.h"


namespace Puzzle {

	class file_manager {

	public:

		static const std::string output_file_name;
		static const std::string error_file_name;

		file_manager() {

		}

		static void output_file(std::string s);

		static void output_error(std::string s);

		static std::vector<piece> input_piece(std::string file_name);

		//frame
		static frame input_frame(std::string file_name);
		
		//output frame and piece
		static void output_figure( const std::vector<Puzzle::polygon_type> & pieces, std::string file_name);

		void output_state(const Puzzle::frame & f, const std::vector<Puzzle::piece>& pieces, std::string file_name);

	};

}