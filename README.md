# README #

高専プロコン２０１７競技部門用Solver

仙台高専名取 筋肉優先探索

開発メンバー

nasatame

https://twitter.com/nobusan_umikiti

K先輩

yasai氏

k氏

疑問があればnasatameまでどうぞ。

# How do I get set up? #
* VS2015 Update 3
* Bitbucket Source Tree
* Siv3D august 2016 v2
* ZBar
* boost 1.64.0

Source Treeは無くてもいいです。

プロジェクトはSiv3Dで作ってください。

バーコード読み取りのためZBarというアプリケーションを使用しているので、ZBarをダウンロード、インストールしてください。

その後、zbarimg.exeのショートカットを製作し名前を変更zbarimg.lnkというそのままのものをこのプロジェクト実行ファイルから見える場所においてください。

boostはヘッダだけで動きます。

# Other #

* 機能

形状情報　読み込み機能　写真撮影（WebCamera必須、ここは実際に使用したままにしているから調整が必要）

配置情報　読み込み機能　探索とめて使ってね。責任取れないよ。

バーコード解析　ZBarというソフトに丸投げ。投げ方はsystem("");その後ファイルで読み込み。

解計算　ピース、フレームの合成処理など計算幾何boostに丸投げ。

解の表示　唯一自前実装

* 使い方

基本ボタンに書いてあるとおり。

最新の探索状況は現在列挙終了した、なかで最新のものを見れる。eraseは見ている状態の削除

保持している探索状況は、自分で選択して保持した状態をコントロールできる。

target_addすると見ている状態を右上に保持できる。（この状態いいというのを保存するのに使う）

target_clearは文字通り保持状態をクリアするのに使う。

search_endは現在実行中の探索を強制終了して、保持している探索状況のほうを使って再び探索を開始できる。

Historyは過去に１度でも自分が保持した状態が全て保存される場所。まさに歴史。

copyで保持しているほうにコピーすることが出来る。

停止ってなってるトグルボタンは、探索の停止と実行が出来るボタン。３秒ほど反映にかかるので右のprogressを見て状況を判断。

101%なら探索開始直後

1-100%なら探索中

0%なら終了

-1%なら列挙できるものがなくなった。かつ答えにたどり着かないどんずまり。正解につながる枝を切ってしまった。


Restartボタン。初期状態が保持されている探索状態のキューに入る。

カドや辺の真ん中をクリックするとそこに直接組み付けることもできる。はずす事は出来ない。

これいじょうは、いろいろいじってください。（ごめん、めんどくさかった）

後ブログさがして動画見て。


* 反省

探索中にGUIとまるの嫌だったからスレッドごとに分けたっけよく考えずに分けたせいで結合がいたるところに出来たりして気持ち悪くなった。（特にポインタ周りがアンスレッドセーフなのにスレッドにぶち込んでる。）

うーん、これどうなってるんだろうというのが頻繁にある、バグもある。

バグが発生するのは100回に1回ぐらいで再現性も取れなかったから無視して大会に突っ込んだ。多分メモリ周りがやばいんだと思う。正直心当たりがありすぎてどうしよう。

命名規則が乱れまくった。日によって変わってたっぽい。

先輩をないがしろにしすぎた。（嫌いなわけじゃないんだよ。）

# License #

著作権などは、製作者の４人に帰属します。
学習及び高専プロコン用途に限り、良識の範囲内で自由にお使いください。

