#include "frame.h"

//frame用 getComposition
//最終的に分割して表示するための要。
std::vector<Puzzle::polygon_type> Puzzle::frame::getComposition()
{
	if (A.use_count() == 0 || B.use_count() == 0) {

		//最下層においてはpolygonをそのまま返せばいいと思われる。
		//表示されるのがうざいのでフレームの外枠は返さないようにします。
		//return std::vector<Puzzle::polygon_type>(1, polygon);
		return std::vector<Puzzle::polygon_type>();
	}

	//A,BのgetCompositionを呼び出す。
	std::vector<Puzzle::polygon_type> vecA = (*A).getComposition();
	std::vector<Puzzle::polygon_type> vecB = (*B).getComposition();

	//vecAにそれぞれから返されたポリゴンをあわせている。
	std::copy(vecB.begin(), vecB.end(), std::back_inserter(vecA));

	return vecA;
}

bool Puzzle::canSimply(frame & A)
{

	polygon_type B;

	B.outer().push_back({ -1,-1 });
	B.outer().push_back({ -1,105 });
	B.outer().push_back({ 105,105 });
	B.outer().push_back({ 105,-1 });
	B.outer().push_back({ -1,-1 });

	for (unsigned inner_num = 0; inner_num < A.inners().size(); inner_num++) {

		//B.outer().push_back(A.outer()[0]);

		B.inners().push_back(Puzzle::polygon_type::ring_type());

		auto v0 = A.inners()[inner_num][A.inners()[inner_num].size() - 2];
		auto v1 = A.inners()[inner_num][0];
		auto v2 = A.inners()[inner_num][1];

		auto isCollinear = [&]() {
			if ((v1.y() - v0.y())*(v2.x() - v0.x()) == (v2.y() - v0.y())*(v1.x() - v0.x())) {
				return true;
			}
			else {
				return false;
			}
		};

		if (isCollinear()) {

		}
		else {
			B.inners()[inner_num].push_back(v1);
		}

		for (unsigned i = 2; i < A.inners()[inner_num].size(); i++) {
			v0 = A.inners()[inner_num][i - 2];
			v1 = A.inners()[inner_num][i - 1];
			v2 = A.inners()[inner_num][i];

			if (isCollinear()) {

			}
			else {
				B.inners()[inner_num].push_back(v1);
			}

		}

		B.inners()[inner_num].push_back(B.inners()[inner_num][0]);

	}

	A.polygon = B;

	return true;
}

//ポリゴン用のコンバーター
Puzzle::polygon_type_double Puzzle::convertePolygon(Puzzle::polygon_type A) {

	Puzzle::polygon_type_double result;

	for (const auto & t : A.outer()) {
		result.outer().push_back({ (double)t.get<0>() , (double)t.get<1>() });
	}

	for (const auto & t1 : A.inners()) {
		result.inners().push_back(Puzzle::polygon_type_double::ring_type());
		for (const auto & t2 : t1) {
			result.inners()[result.inners().size() - 1].push_back({ (double)t2.get<0>(),(double)t2.get<1>()});
		}
	}

	return result;

}

Puzzle::polygon_type Puzzle::convertePolygon(Puzzle::polygon_type_double A) {

	Puzzle::polygon_type result;


	for (const auto & t : A.outer()) {
		result.outer().push_back({ (int)t.get<0>() , (int)t.get<1>() });
	}

	for (const auto & t1 : A.inners()) {
		result.inners().push_back(Puzzle::polygon_type::ring_type());
		for (const auto & t2 : t1) {
			result.inners()[result.inners().size() - 1].push_back({ (int)t2.get<0>(),(int)t2.get<1>() });
		}
	}

	return result;

}

//これ以下のunionBySide,Degreeシリーズは初期に使ってたもので現状使っていない。
//argument= frame piece frame_inner_num vertex_A vertex_B
Puzzle::frame Puzzle::unionBySide(frame A, piece B,int inum, int va, int vb)
{
	/*
	frameとpieceを合成する。操作。

	内部は分断される可能性があるため、注意が必要。
		
	*/

	std::pair<int, int> target(A.inners()[inum][va].y() - A.inners()[inum][va + 1].y(), A.inners()[inum][va].x() - A.inners()[inum][va + 1].x());

	while (target != std::pair<int, int>(B.outer()[vb + 1].y() - B.outer()[vb].y(), B.outer()[vb + 1].x() - B.outer()[vb].x())) {
		canRotate(B);
		if (B.ope.rotateTheta == 0) return frame();
	}

	//移動
	//フレームは初期位置から動かすな！！注意。
	
	//最終的にA(1-2),B(1-2)が上手くくっつくように移動する。

	canMove(B, A.inners()[inum][va + 1].x() - B.outer()[vb].x(), A.inners()[inum][va + 1].y() - B.outer()[vb].y());

	//エラー制御とりあえず面積を見て足されたか確認すればOKかと思われる。
	//これを潜り抜けるパターンが存在することが分かった。

	std::vector<polygon_type_double> vec_poly;
	polygon_type_double Apoly = convertePolygon(A.polygon);
	polygon_type_double Bpoly = convertePolygon(B.polygon);

	boost::geometry::union_(Apoly, Bpoly, vec_poly);

	//図形数からエラーを検出する。
	if (vec_poly.size() == 1) {

	}
	else {
		return frame();
	}

	//面積的な面からエラーを検出する。
	if (std::abs(boost::geometry::area(A.polygon) + boost::geometry::area(B.polygon) - boost::geometry::area(vec_poly[0]))<0.0000000001) {

	}
	else {
		return frame();
	}

	boost::geometry::correct(vec_poly[0]);

	//返すピースのA,Bのshared_ptrに親となった二つのピースを登録する。

	frame result(convertePolygon(vec_poly[0]), std::shared_ptr<frame>(new frame(A, true)), std::shared_ptr<piece>(new piece(B, true)));

	canSimply(result);

	return result;

}

bool Puzzle::unionBySide(frame A, piece B, int inum, int va, int vb, frame & result)
{

	/*
	frameとpieceを合成する。操作。

	内部は分断される可能性があるため、注意が必要。

	*/

	std::pair<int, int> target(A.inners()[inum][va].y() - A.inners()[inum][va + 1].y(), A.inners()[inum][va].x() - A.inners()[inum][va + 1].x());

	while (target != std::pair<int, int>(B.outer()[vb + 1].y() - B.outer()[vb].y(), B.outer()[vb + 1].x() - B.outer()[vb].x())) {
		canRotate(B);
		if (B.ope.rotateTheta == 0) return false;
	}

	//移動
	//フレームは初期位置から動かすな！！注意。

	//最終的にA(1-2),B(1-2)が上手くくっつくように移動する。

	canMove(B, A.inners()[inum][va + 1].x() - B.outer()[vb].x(), A.inners()[inum][va + 1].y() - B.outer()[vb].y());

	//エラー制御とりあえず面積を見て足されたか確認すればOKかと思われる。
	//これを潜り抜けるパターンが存在することが分かった。

	std::vector<polygon_type_double> vec_poly;
	polygon_type_double Apoly = convertePolygon(A.polygon);
	polygon_type_double Bpoly = convertePolygon(B.polygon);

	boost::geometry::union_(Apoly, Bpoly, vec_poly);

	//図形数からエラーを検出する。
	if (vec_poly.size() == 1) {

	}
	else {
		return false;
	}

	//面積的な面からエラーを検出する。
	if (std::abs(boost::geometry::area(A.polygon) + boost::geometry::area(B.polygon) - boost::geometry::area(vec_poly[0]))<0.0000000001) {

	}
	else {
		return false;
	}

	boost::geometry::correct(vec_poly[0]);

	//返すピースのA,Bのshared_ptrに親となった二つのピースを登録する。

	result = frame(convertePolygon(vec_poly[0]), std::shared_ptr<frame>(new frame(A, true)), std::shared_ptr<piece>(new piece(B, true)));

	canSimply(result);



	return true;
}

//初期に使ってた。効率がいちいちコピーするから悪すぎるんだよなあ。
Puzzle::frame Puzzle::unionByDeg(frame A, piece B, int inum, int va, int vb)
{
	while (!divEqual(A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())].y() - A.inners()[inum][correctSuffix(va, A.inners()[inum].size())].y(),
		A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())].x() - A.inners()[inum][correctSuffix(va, A.inners()[inum].size())].x(),
		B.outer()[correctSuffix(vb, B.outer().size())].y() - B.outer()[correctSuffix(vb + 1, B.outer().size())].y(),
		B.outer()[correctSuffix(vb, B.outer().size())].x() - B.outer()[correctSuffix(vb + 1, B.outer().size())].x()
	)) {
		canRotate(B);
		if (B.ope.rotateTheta == 0)return frame();
	}

	//移動
	//フレームは初期位置から動かすな！！注意。

	//最終的にA(1-2),B(1-2)が上手くくっつくように移動する。

	canMove(B, A.inners()[inum][va].x() - B.outer()[vb].x(), A.inners()[inum][va].y() - B.outer()[vb].y());

	//エラー制御とりあえず面積を見て足されたか確認すればOKかと思われる。
	//これを潜り抜けるパターンが存在することが分かった。

	std::vector<polygon_type_double> vec_poly;
	polygon_type_double Apoly = convertePolygon(A.polygon);
	polygon_type_double Bpoly = convertePolygon(B.polygon);

	boost::geometry::union_(Apoly, Bpoly, vec_poly);

	//図形数からエラーを検出する。
	if (vec_poly.size() == 1) {

	}
	else {
		return frame();
	}

	//面積的な面からエラーを検出する。
	if (std::abs(boost::geometry::area(A.polygon) + boost::geometry::area(B.polygon) - boost::geometry::area(vec_poly[0]))<0.0000000001) {

	}
	else {
		return frame();
	}

	boost::geometry::correct(vec_poly[0]);

	//返すピースのA,Bのshared_ptrに親となった二つのピースを登録する。

	frame result(convertePolygon(vec_poly[0]), std::shared_ptr<frame>(new frame(A, true)), std::shared_ptr<piece>(new piece(B, true)));

	canSimply(result);

	return result;

}

bool Puzzle::unionByDeg(frame A, piece B, int inum, int va, int vb, frame & result)
{
	while (!divEqual(
		A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())].y() - A.inners()[inum][correctSuffix(va, A.inners()[inum].size())].y(),
		A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())].x() - A.inners()[inum][correctSuffix(va, A.inners()[inum].size())].x(),
		B.outer()[correctSuffix(vb, B.outer().size())].y() - B.outer()[correctSuffix(vb + 1, B.outer().size())].y(),
		B.outer()[correctSuffix(vb, B.outer().size())].x() - B.outer()[correctSuffix(vb + 1, B.outer().size())].x()
	)) {
		canRotate(B);
		if (B.ope.rotateTheta == 0)return false;
	}

	//移動
	//フレームは初期位置から動かすな！！注意。

	//最終的にA(1-2),B(1-2)が上手くくっつくように移動する。

	canMove(B, A.inners()[inum][va].x() - B.outer()[vb].x(), A.inners()[inum][va].y() - B.outer()[vb].y());

	//エラー制御とりあえず面積を見て足されたか確認すればOKかと思われる。
	//これを潜り抜けるパターンが存在することが分かった。

	std::vector<polygon_type_double> vec_poly;
	polygon_type_double Apoly = convertePolygon(A.polygon);
	polygon_type_double Bpoly = convertePolygon(B.polygon);

	boost::geometry::union_(Apoly, Bpoly, vec_poly);

	//図形数からエラーを検出する。
	if (vec_poly.size() == 1) {

	}
	else {
		return false;
	}

	//面積的な面からエラーを検出する。
	if (std::abs(boost::geometry::area(A.polygon) + boost::geometry::area(B.polygon) - boost::geometry::area(vec_poly[0]))<0.0000000001) {

	}
	else {
		return false;
	}

	boost::geometry::correct(vec_poly[0]);

	//返すピースのA,Bのshared_ptrに親となった二つのピースを登録する。

	result = frame(convertePolygon(vec_poly[0]), std::shared_ptr<frame>(new frame(A, true)), std::shared_ptr<piece>(new piece(B, true)));

	canSimply(result);

	return true;
}

Puzzle::polygon_type::point_type subtract_point(const Puzzle::polygon_type::point_type& a,const Puzzle::polygon_type::point_type& b) {
	return Puzzle::polygon_type::point_type(a.x()-b.x(),a.y()-b.y());
}

//内角を返す。重要
double Puzzle::getInteriorAngle(const frame & A, int inum, const int va)
{
	//atan2で殴る。外積で殴るより簡単。

	polygon_type::point_type vectorA = subtract_point(A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())], A.inners()[inum][correctSuffix(va, A.inners()[inum].size())]);
	polygon_type::point_type vectorB = subtract_point(A.inners()[inum][correctSuffix(va + 1, A.inners()[inum].size())], A.inners()[inum][correctSuffix(va, A.inners()[inum].size())]);

	double result = atan2(vectorB.y(), vectorB.x()) - atan2(vectorA.y(), vectorA.x());

	if (result < 0) {
		result += M_PI * 2;
	}
	else if (result >= M_PI * 2) {
		result -= M_PI * 2;
	}

	return result;
}

double Puzzle::getExternalAngle(const frame & A, int inum, const int va) {
	//atan2で殴る。外積で殴るより簡単。
	polygon_type::point_type vectorA = subtract_point(A.inners()[inum][correctSuffix(va - 1, A.inners()[inum].size())], A.inners()[inum][correctSuffix(va, A.inners()[inum].size())]);
	polygon_type::point_type vectorB = subtract_point(A.inners()[inum][correctSuffix(va + 1, A.inners()[inum].size())], A.inners()[inum][correctSuffix(va, A.inners()[inum].size())]);

	double result = atan2(vectorA.y(), vectorA.x()) - atan2(vectorB.y(), vectorB.x());

	if (result < 0) {
		result += M_PI * 2;
	}
	else if (result >= M_PI * 2) {
		result -= M_PI * 2;
	}

	return result;
}


//K先輩のコード俺は知らん。
int Puzzle::frame::getNewPieceEdgePercent(frame& f) {

	int res = 0;
	typedef boost::geometry::model::d2::point_xy<int> point;
	std::vector<polygon_type> out;
	polygon_type target;

	boost::geometry::difference(this->polygon, f.polygon, out);
	if (out.size() == 1)target = out[0];
	else return -100;//error

	for (int i = 0; i < f.polygon.inners().size(); i++) {
		for (int j = 0; j < f.polygon.inners()[i].size() - 1; j++) {
			boost::geometry::model::linestring<point> Aline = boost::assign::list_of<point>(f.polygon.inners()[i][j].x(), f.polygon.inners()[i][j].y())(f.polygon.inners()[i][j + 1].x(), f.polygon.inners()[i][j + 1].y());
			if (boost::geometry::intersects(Aline, target)) {
				for (int k = 0; k < target.outer().size() - 1; k++) {
					boost::geometry::model::linestring<point> Tline = boost::assign::list_of<point>(target.outer()[k].x(), target.outer()[k].y())(target.outer()[k + 1].x(), target.outer()[k + 1].y());

					double Adis = boost::geometry::distance(f.polygon.inners()[i][j], f.polygon.inners()[i][j + 1]);
					double Tdis = boost::geometry::distance(target.outer()[k], target.outer()[k + 1]);
					int intersection = intersectsByKitajima(Aline, Tline);
					if ((std::abs(intersection - Tdis) < 0.01) && (std::abs(intersection - Adis) < 0.01)) res += 200;
					else {
						double minAT = std::min(Adis, Tdis);
						double maxAT = std::max(Adis, Tdis);
						res += (minAT / maxAT) * 100;
					}
				}
			}
		}
	}

	return res;
}

//なつかしー移動日の夜にメッチャ頑張って書いた気がする
double Puzzle::frame::intersectsByKitajima(boost::geometry::model::linestring<boost::geometry::model::d2::point_xy<int>> A, boost::geometry::model::linestring<boost::geometry::model::d2::point_xy<int>> B) {
	//交差判定
	bool intersect = boost::geometry::intersects(A, B);
	if (!intersect)return 0;

	//傾き判定
	double Aslope = 0, Bslope = 0;
	if (!(std::abs(A[1].y() - A[0].y()) < 0.01) && !(std::abs(B[1].y() - B[0].y()) < 0.01)) {
		Aslope = std::abs((A[1].x() - A[0].x()) / std::abs(A[1].y() - A[0].y()));
		Bslope = std::abs((B[1].x() - B[0].x()) / std::abs(B[1].y() - B[0].y()));
	}
	else {
		Aslope = std::abs((A[1].y() - A[0].y()) / std::abs(A[1].x() - A[0].x()));
		Bslope = std::abs((B[1].y() - B[0].y()) / std::abs(B[1].x() - B[0].x()));
	}

	//if (!(std::abs(Aslope - Bslope) < 0.01)) return 0;

	if (
		std::abs(
		(A[1].y() - A[0].y()) / std::sqrt(std::pow(A[1].y() - A[0].y(), 2) + std::pow(A[1].x() - A[0].x(), 2)) -
			(B[1].y() - B[0].y()) / std::sqrt(std::pow(B[1].y() - B[0].y(), 2) + std::pow(B[1].x() - B[0].x(), 2))) < 0.001
		&&
		std::abs(
		(A[1].x() - A[0].x()) / std::sqrt(std::pow(A[1].y() - A[0].y(), 2) + std::pow(A[1].x() - A[0].x(), 2)) -
			(B[1].x() - B[0].x()) / std::sqrt(std::pow(B[1].y() - B[0].y(), 2) + std::pow(B[1].x() - B[0].x(), 2))) < 0.001
		) {

	}
	else {
		return 0;
	}

	if (
		std::abs(
		(A[1].y() - A[0].y()) / std::sqrt(std::pow(A[1].y() - A[0].y(), 2) + std::pow(A[1].x() - A[0].x(), 2)) -
			-(B[1].y() - B[0].y()) / std::sqrt(std::pow(B[1].y() - B[0].y(), 2) + std::pow(B[1].x() - B[0].x(), 2))) < 0.001
		&&
		std::abs(
		(A[1].x() - A[0].x()) / std::sqrt(std::pow(A[1].y() - A[0].y(), 2) + std::pow(A[1].x() - A[0].x(), 2)) -
			-(B[1].x() - B[0].x()) / std::sqrt(std::pow(B[1].y() - B[0].y(), 2) + std::pow(B[1].x() - B[0].x(), 2))) < 0.001
		) {

	}
	else {
		return 0;
	}

	if (!(std::abs(A[1].y() - A[0].y()) < 0.01) && !(std::abs(B[1].y() - B[0].y()) < 0.01)) {
		if (((A[0].x() < B[0].x()) && (A[0].x() < B[1].x()) && (A[1].x() > B[0].x()) && (A[1].x() < B[1].x())) ||
			((A[0].x() > B[0].x()) && (A[0].x() > B[1].x()) && (A[1].x() < B[0].x()) && (A[1].x() > B[1].x()))) {
			return boost::geometry::distance(B[0], A[1]);
		}
		if (((A[0].x() < B[0].x()) && (A[0].x() < B[1].x()) && (A[1].x() > B[1].x()) && (A[1].x() < B[0].x())) ||
			((A[0].x() > B[0].x()) && (A[0].x() > B[1].x()) && (A[1].x() < B[1].x()) && (A[1].x() > B[0].x()))) {
			return boost::geometry::distance(B[1], A[1]);
		}
		if (((A[1].x() < B[0].x()) && (A[1].x() < B[1].x()) && (A[0].x() > B[0].x()) && (A[0].x() < B[1].x())) ||
			((A[1].x() > B[0].x()) && (A[1].x() > B[1].x()) && (A[0].x() < B[0].x()) && (A[0].x() > B[1].x()))) {
			return boost::geometry::distance(B[0], A[0]);
		}
		if (((A[1].x() < B[0].x()) && (A[1].x() < B[1].x()) && (A[0].x() > B[1].x()) && (A[0].x() < B[0].x())) ||
			((A[1].x() > B[0].x()) && (A[1].x() > B[1].x()) && (A[0].x() < B[1].x()) && (A[0].x() > B[0].x()))) {
			return boost::geometry::distance(B[1], A[0]);
		}
		if (((A[0].x() < B[0].x()) && (A[0].x() < B[1].x()) && (A[1].x() > B[0].x()) && (A[1].x() > B[1].x())) ||
			((A[1].x() < B[0].x()) && (A[1].x() < B[1].x()) && (A[0].x() > B[0].x()) && (A[0].x() > B[1].x()))) {
			return boost::geometry::distance(B[0], B[1]);
		}
		if (((B[0].x() < A[0].x()) && (B[0].x() < A[1].x()) && (B[1].x() > A[0].x()) && (B[1].x() > A[1].x())) ||
			((B[1].x() < A[0].x()) && (B[1].x() < A[1].x()) && (B[0].x() > A[0].x()) && (B[0].x() > A[1].x()))) {
			return boost::geometry::distance(A[0], A[1]);
		}
	}
	else {
		if (((A[0].y() < B[0].y()) && (A[0].y() < B[1].y()) && (A[1].y() > B[0].y()) && (A[1].y() < B[1].y())) ||
			((A[0].y() > B[0].y()) && (A[0].y() > B[1].y()) && (A[1].y() < B[0].y()) && (A[1].y() > B[1].y()))) {
			return boost::geometry::distance(B[0], A[1]);
		}
		if (((A[0].y() < B[0].y()) && (A[0].y() < B[1].y()) && (A[1].y() > B[1].y()) && (A[1].y() < B[0].y())) ||
			((A[0].y() > B[0].y()) && (A[0].y() > B[1].y()) && (A[1].y() < B[1].y()) && (A[1].y() > B[0].y()))) {
			return boost::geometry::distance(B[1], A[1]);
		}
		if (((A[1].y() < B[0].y()) && (A[1].y() < B[1].y()) && (A[0].y() > B[0].y()) && (A[0].y() < B[1].y())) ||
			((A[1].y() > B[0].y()) && (A[1].y() > B[1].y()) && (A[0].y() < B[0].y()) && (A[0].y() > B[1].y()))) {
			return boost::geometry::distance(B[0], A[0]);
		}
		if (((A[1].y() < B[0].y()) && (A[1].y() < B[1].y()) && (A[0].y() > B[1].y()) && (A[0].y() < B[0].y())) ||
			((A[1].y() > B[0].y()) && (A[1].y() > B[1].y()) && (A[0].y() < B[1].y()) && (A[0].y() > B[0].y()))) {
			return boost::geometry::distance(B[1], A[0]);
		}
		if (((A[0].y() < B[0].y()) && (A[0].y() < B[1].y()) && (A[1].y() > B[0].y()) && (A[1].y() > B[1].y())) ||
			((A[1].y() < B[0].y()) && (A[1].y() < B[1].y()) && (A[0].y() > B[0].y()) && (A[0].y() > B[1].y()))) {
			return boost::geometry::distance(B[0], B[1]);
		}
		if (((B[0].y() < A[0].y()) && (B[0].y() < A[1].y()) && (B[1].y() > A[0].y()) && (B[1].y() > A[1].y())) ||
			((B[1].y() < A[0].y()) && (B[1].y() < A[1].y()) && (B[0].y() > A[0].y()) && (B[0].y() > A[1].y()))) {
			return boost::geometry::distance(A[0], A[1]);
		}
	}

	return -100;
}

