#include"procon2017.h"
#include"piece.h"
#include"prepare.h"
#include"prepare_before.h"

//T,K後輩の力作。回転関数。私は関知していないけど。バグ無く動いて凄いよかった。

//2点の角度を算出
double get_degree(std::array<int, 2> vec_point) {
	assert(!(vec_point[0] == 0 && vec_point[1] == 0));
	double ret = Puzzle::degree_map.find(vec_point)->second;
	return ret;
}

auto search_range(std::array<int, 2> base_point, std::array<int, 2> move_point) {
	int hypotenuse = (move_point[0] - base_point[0]) * (move_point[0] - base_point[0]) + (move_point[1] - base_point[1]) * (move_point[1] - base_point[1]);
	auto ret = Puzzle::range_map.find(hypotenuse);
	//ret は Puzzle::range_map.end()じゃないってことを明示的に示している。
	assert(ret != Puzzle::range_map.end());

	return ret;
}

//!(A and B) == !A or !B

std::array<int, 2> rotate_point(std::array<int, 2> base_point, std::array<int, 2> move_point, double move_degree) {
	std::array<int, 2> vec_point = { move_point[0] - base_point[0], move_point[1] - base_point[1] };
	if (vec_point[0] == 0 && vec_point[1] == 0) {
		return base_point;
	}
	//10のマイナス7乗
	const double allowable_error = 10e-7;
	//渡された角度に回転させたい座標を足す
	//x軸からの回転に戻す
	double total_degree = move_degree + get_degree(vec_point);
	if (total_degree >= 2 * Puzzle::M_PI) {
		total_degree -= 2 * Puzzle::M_PI;
	}
	//斜辺の長さを取得
	double hypotenuse_true = std::hypot(vec_point[0], vec_point[1]);
	//sin cos を算出
	double interim_y = hypotenuse_true * sin(total_degree);
	double interim_x = hypotenuse_true * cos(total_degree);
	int ans_y = -1000;
	int ans_x = -1000;
	//誤差っぽいやつ…??
	if (std::abs(std::round(interim_y) - interim_y) < allowable_error)ans_y = (int)std::round(interim_y);
	if (std::abs(std::round(interim_x) - interim_x) < allowable_error)ans_x = (int)std::round(interim_x);
	std::array<int, 2> ret;
	ret.fill(-1000);
	if (ans_y != -1000 && ans_x != -1000) {
		ret = { ans_x + base_point[0],ans_y + base_point[1] };
	}
	return ret;
}

//1個め
bool Puzzle::canRotate(piece & A) {
	//マップ生成
	if (range_map.size() == 0) {
		Puzzle::create_map_calc();
	}

	//初点と次点を取得し保存
	std::array<int, 2> first_point = { A.outer()[0].x(), A.outer()[0].y() };
	std::array<int, 2> second_point = { A.outer()[1].x(), A.outer()[1].y() };

	//初点と次点の回りうる整数座標を全算出
	auto rotate_pos = search_range(first_point, second_point);

	//この時点で原点は初点へと移っている
	//以降、初点を相対原点とし計算を行う

	//初点と次点の角度
	double stadard_angle = get_degree({ A.outer()[1].x() - A.outer()[0].x(),A.outer()[1].y() - A.outer()[0].y() });

	//それぞれの座標に対する軸(初点と次点)からの角度
	std::vector<double> angle;
	double angle_back = 0.0;

	//全座標→角度への変換
	for (auto itr = rotate_pos->second.begin(); itr != rotate_pos->second.end(); itr++) {
		//原点からの角度のため、軸からの角度に治す
		angle_back = get_degree(*itr) - stadard_angle;
		if (angle_back < 0)angle_back += 2 * M_PI;
		//0は同じため、弾いている
		if (angle_back != 0) {
			angle.push_back(angle_back);
		}
	}

	//角度を小さい順に並べる
	std::sort(angle.begin(), angle.end());

	//判定用変数
	unsigned judge = 1;
	std::array<int, 2> box;
	std::vector<std::array<int, 2>>  figure;
	double min_rotate = 0;

	//回転
	for (double digree : angle) {
		judge = 0;
		figure.clear();
		//figure.push_back({ A.outer()[0].x(),A.outer()[0].y() });
		for (unsigned pos = 0; pos < A.outer().size(); pos++) {
			box = rotate_point({ A.outer()[0].x(),A.outer()[0].y() }, { A.outer()[pos].x(),A.outer()[pos].y() }, digree);
			if (!(box[0] == -1000)) {
				figure.push_back(box);
				judge++;
			}
			else {
				figure.clear();
				break;
			}
		}
		if (judge == A.outer().size()) {
			min_rotate = digree;
			break;
		}
	}

	//pieceに代入
	int i = 0;
	for (auto & v : A.outer()) {
		v.set<0>(figure[i][0]);
		v.set<1>(figure[i][1]);
		i++;
	}

	A.ope.rotateX = A.outer()[0].x();
	A.ope.rotateY = A.outer()[0].y();
	//ここで保存されるのは変位分の角度でしかない。
	A.ope.rotateTheta += min_rotate;

	//2piまたは-2pi回転していたら。
	if (std::abs(A.ope.rotateTheta - 2 * Puzzle::M_PI) < 0.0000001 ||
		std::abs(A.ope.rotateTheta + 2 * Puzzle::M_PI) < 0.0000001) {
		A.ope.rotateTheta = 0;
	}

	return true;
}

//復元のほう
bool Puzzle::canRotate(polygon_type & A, int axis_x, int axis_y, double angle) {

	//保存のやつ
	std::vector<std::array<int, 2>>  figure;

	std::array<int, 2> box;

	//回すやつ
	for (unsigned i = 0; i < A.outer().size(); i++) {
		box = rotate_point({ axis_x,axis_y }, { A.outer()[i].x(),A.outer()[i].y() }, angle);
		if (!(box[0] == -1000))figure.push_back(box);
		else return false;
	}

	//pieceに代入
	int i = 0;
	for (auto & v : A.outer()) {
		v.set<0>(figure[i][0]);
		v.set<1>(figure[i][1]);
		i++;
	}

	return true;
}