#pragma once

#include<map>
#include<vector>
#include<array>
#include<fstream>
#include<unordered_map>
#include"piece.h"
#include"beam_search.h"
//#define M_PI 3.14159265358979323846

namespace Puzzle {

	static bool operator==(const std::array<int, 2> &a1,const std::array<int, 2>& a2) {
		return (a1[0] == a2[0]) && (a1[1] == a2[1]);
	}

	static bool operator!=(const std::array<int, 2> &a1, const std::array<int, 2>& a2) {
		return (a1[0] != a2[0]) || (a1[1] != a2[1]);
	}

	struct Hash {
		typedef std::size_t result_type;

		std::size_t operator()(const std::array<int, 2>& key) const;
	};

	inline std::size_t Hash::operator()(const std::array<int, 2>& key) const {
		return key[0] * 1000 + key[1];
	}


	//key:斜辺 value:座標
	static std::unordered_map<int, std::vector<std::array<int, 2>>> range_map;
	//key:座標 value:角度
	static std::unordered_map<std::array<int, 2>, double,Hash> degree_map;

	//準備段階でピース同士を接続する。
	//できたらよかったね
	std::vector<Puzzle::state> connectPiece(const Puzzle::state & pieces);

}