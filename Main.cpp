﻿# include <Siv3D.hpp>
# include "piece.h"
# include "procon2017.h"
# include "beam_search.h"
# include "gui.h"
# include "file_io.h"
# include "prepare.h"

/*
高専プロコン2017 仙台高等専門学校　名取キャンパス　筋肉優先探索

Solver及び全部

機能
形状情報　読み込み機能　写真撮影（WebCamera必須、ここは実際に使用したままにしているから調整が必要）
配置情報　読み込み機能　探索とめて使ってね。責任取れないよ。

バーコード解析　ZBarというソフトに丸投げ。投げ方はsystem("");その後ファイルで読み込み。
解計算　ピース、フレームの合成処理など計算幾何boostに丸投げ。
解の表示　唯一自前実装

使い方

基本ボタンに書いてあるとおり。

最新の探索状況は現在列挙終了した、なかで最新のものを見れる。eraseは見ている状態の削除

保持している探索状況は、自分で選択して保持した状態をコントロールできる。
target_addすると見ている状態を右上に保持できる。（この状態いいというのを保存するのに使う）
target_clearは文字通り保持状態をクリアするのに使う。
search_endは現在実行中の探索を強制終了して、保持している探索状況のほうを使って再び探索を開始できる。

Historyは過去に１度でも自分が保持した状態が全て保存される場所。まさに歴史。
copyで保持しているほうにコピーすることが出来る。

停止ってなってるトグルボタンは、探索の停止と実行が出来るボタン。３秒ほど反映にかかるので右のprogressを見て状況を判断。
101%なら探索開始直後
1-100%なら探索中
0%なら終了
-1%なら列挙できるものがなくなった。かつ答えにたどり着かないどんずまり。正解につながる枝を切ってしまった。

Restartボタン。初期状態が保持されている探索状態のキューに入る。

カドや辺の真ん中をクリックするとそこに直接組み付けることもできる。はずす事は出来ない。

これいじょうは、いろいろいじってください。（ごめん、めんどくさかった）
後ブログさがして動画見て。

反省
探索中にGUIとまるの嫌だったからスレッドごとに分けたっけよく考えずに分けたせいで結合がいたるところに出来たりして気持ち悪くなった。（特にポインタ周りがアンスレッドセーフなのにスレッドにぶち込んでる。）
うーん、これどうなってるんだろうというのが頻繁にある、バグもある。
バグが発生するのは100回に1回ぐらいで再現性も取れなかったから無視して大会に突っ込んだ。多分メモリ周りがやばいんだと思う。正直心当たりがありすぎてどうしよう。
命名規則が乱れまくった。日によって変わってたっぽい。


*/

void Main()
{
	
	//画面の解像度依存。デスクトップＰＣ向けに調整
	Window::Resize(1900, 1000);


	//表示関連画面サイズに合わせていろいろ変えて
	const Font font(20);
	Puzzle::gui g_now;
	g_now.gui_magnification = 8;
	Puzzle::gui g_target;
	g_target.gui_magnification = 4;
	g_target.gui_move_x = 8 * 100 + 50;
	Puzzle::gui history;
	history.gui_move_x = 8 * 100 + 50;
	history.gui_move_y = 250;
	history.gui_magnification = 4;

	Puzzle::gui viewer_piece;
	viewer_piece.gui_magnification = 2;


	//////////////////一般的な起動用構成/////////////////
	//return : 0 なら読み取れた
	//return : 1 ファイルが見つからない
	//return : 4 バーコードが読み取れない
	////Enterキーで読み取り、Qで終了。
	//

	//ここのコメントをはずして下のやつをコメントアウトするとWebカメラから撮影できるようになる。
	//通常はカメラ２台目を読むようにしている（１台目はノーパソの正面カメラのため）
/*
	std::vector<Puzzle::piece> p;
	Puzzle::frame F;

	Puzzle::gui().takePicture();

	for (int i = 1;; i++) {
	TextReader tr(L"cameraData" + ToString(i) + L".txt", TextEncoding::ANSI);

	if (!tr.isOpened()) {
	F = Puzzle::file_manager::input_frame("cameraData" + std::to_string(i-1) + ".txt");
	break;
	}

	tr.close();
	std::vector<Puzzle::piece> temp = Puzzle::file_manager::input_piece("cameraData" + std::to_string(i) + ".txt");
	copy(temp.begin(),temp.end(),std::back_inserter(p));

	}

	ClearPrint();
*/
	/////////////////////////////////////////////////////

	///////////////通常デバッグ用の構成////////////////
/*
	std::vector<Puzzle::piece> p = Puzzle::file_manager::input_piece("sample.txt");
	Puzzle::frame F = Puzzle::file_manager::input_frame("sample.txt");
*/

	std::vector<Puzzle::piece> p = Puzzle::file_manager::input_piece("kame.txt");
	Puzzle::frame F = Puzzle::file_manager::input_frame("kame.txt");

	/////////////////////////////////////////////////////

	if (p.size() == 0) { 
		Puzzle::file_manager::output_error("ピース読み込み失敗。");
		return;
	}
	else {
		Puzzle::file_manager::output_error("ピース読み込み完了。");
	}

	if (F.inners().size() == 0) {
		Puzzle::file_manager::output_error("フレーム読み込み失敗。");
		return;
	}
	else {
		Puzzle::file_manager::output_error("フレーム読み込み完了。");
	}


	Stopwatch sw;
	Stopwatch tag_sw;
	Stopwatch swDelay;

	Stopwatch gameTimer;
	gameTimer.start();

	sw.start();
	tag_sw.start();
	swDelay.start();

	int count = 0;

	std::vector<Puzzle::polygon_type> vec_result;


	std::vector<Puzzle::state> beforeState;
	std::vector<Puzzle::state> nowState;
	std::vector<Puzzle::state> nextState;
	std::vector<Puzzle::state> targetState;
	std::vector<Puzzle::state> historyState;

	//つわものどもの夢の跡。ピースに前処理しようとして間に合いませんでした。
	//nowState = Puzzle::connectPiece(Puzzle::state(F, p));

	//初期化
	//ピースの番号も併せて初期化している。

	//ここらへんsingletonをただのグローバル変数として使っているから闇が深い
	Puzzle::piece_manager::get_instance().initialize(p);
	//この初期化は必要なくなったので消しました。
	//Puzzle::excludeState(nowState);

	nowState.push_back(Puzzle::state(F, p));
	beforeState.push_back(Puzzle::state(F, p));

	//初期化終了

	std::future<int> f;

	std::mutex mtx;
	int progress = 0;
	int shareProgress = 0;

	int nowStateSize = 0;

	Puzzle::beam_search bs;

	Puzzle::file_manager::output_error(std::to_string(count) + ":" + std::to_string(nowState.size()));
	Puzzle::file_manager::output_error("開始しました。");

	//ＧＵＩいらないときはanswer_count = 1にしてコメントのほうを使うと出来ます。
	Puzzle::state answer;// = Puzzle::beam_search::chokudaiSearchBySuzuki(F, p);
	int answer_count = 0;

	int view_page = 0;
	int target_view_page = 0;
	int history_view_page = 0;
	bool target_flag = false;

	//写真撮ると解像度変わるから戻してる。GUIと密結合きもちわるい。
	Window::Resize(1900, 1000);


	//GUIの設定

	GUI gui(GUIStyle::Default);
	//gui.add(L"x", GUISlider::Create(50.0, 2000.0, 320.0, 200));
	gui.add(L"now_text", GUIText::Create(L"最新の探索状況"));
	gui.add(L"prev_view", GUIButton::Create(L"prev_view"));
	gui.add(L"next_view", GUIButton::Create(L"next_view"));
	gui.add(L"auto", GUIToggleSwitch::Create(L"auto OFF", L"ON", false));
	gui.addln(L"bef_erase", GUIButton::Create(L"erase"));
	gui.add(L"target_text", GUIText::Create(L"保持している探索状況"));
	gui.add(L"tag_prev_view", GUIButton::Create(L"prev_view"));
	gui.add(L"tag_next_view", GUIButton::Create(L"next_view"));
	gui.addln(L"tag_auto", GUIToggleSwitch::Create(L"auto OFF", L"ON", false));
	gui.add(L"add", GUIButton::Create(L"target add"));
	gui.add(L"end", GUIButton::Create(L"search end"));
	gui.addln(L"clear", GUIButton::Create(L"target clear"));
	gui.add(L"history_text", GUIText::Create(L"Historyの管理"));
	gui.add(L"his_prev_view", GUIButton::Create(L"prev_view"));
	gui.add(L"his_next_view", GUIButton::Create(L"next_view"));
	gui.addln(L"his_copy", GUIButton::Create(L"history copy"));
	gui.addln(L"advance_state", GUIToggleSwitch::Create(L"停止", L"実行", false));
	gui.addln(L"restart", GUIButton::Create(L"Restart"));


	GUI guiSearch(GUIStyle::Default);
	guiSearch.add(L"target_policy_text", GUIText::Create(L"探索方針"));
	guiSearch.add(L"search_policy", GUIRadioButton::Create({ L"Beam" , L"Chokudai",L"Full", L"BeamStack" }, 0u, true));
	//自作の超探索方針
	//FullPlus
	//序盤間違えないように探索。おおきめに状態を持っておく
	//中終盤内部分割されたら小さいものから入れていくように最適化
	//１０ピース全探索
	guiSearch.add(L"func_policy", GUIRadioButton::Create({ L"ALL", L"Deg", L"DegPlus" , L"Side" , L"SidePlus" , L"SideDegPlus"}, 2u, true));
	//Onlyはコメントアウトっていって１箇所しか合致しなかったらってやつも考えた（常識的に不可能な没ネタ）
	guiSearch.add(L"eval_policy", GUIRadioButton::Create({ L"Edge", L"EdgePlus", L"EdgePercentPlus" 
		, L"EdgePlus&Hole", L"None", L"Exclusion", L"ByKitajima" , L"ByKitajima2" }, 0u, true));
	//guiSearch.addln(L"small_policy", GUIToggleSwitch::Create(L"Small OFF", L"Small ON", false));//一定以下の大きさのピースをはじく設定（没ネタ）
	
	gui.setPos(0, Window::Size().y - gui.getRect().h - 20);
	guiSearch.setPos(gui.getRect().w, Window::Size().y - guiSearch.getRect().h - 20);
	Window::SetStyle(WindowStyle::Sizeable);

	//GUI関係の変数。文字表示の全体位置をいじれる
	const int font_left_pos_x = 800;
	const int font_left_pos_y = 400;
	const int font_interval = 30;

	while (System::Update())
	{
		font(L"progress : ", progress, L"%").draw(font_left_pos_x, font_left_pos_y + font_interval * 0);

		//Frameの枠線を表示
		g_now.outputInners(F, 2, ColorF(Palette::White, 50));

		//初期状態をtargetStateに追加する。間違って押すなよ
		if (gui.button(L"restart").pushed) {

			targetState.clear();
			targetState.push_back(Puzzle::state(F, p));
			target_flag = false;
			target_view_page = 0;

			g_now.gui_magnification = 8;
			answer_count = 0;
		}

		//答えが発見されたら
		if (answer_count > 0) {

			if (vec_result.size() == 0) {
				vec_result = answer.main_frame.getComposition();
			}

			g_now.gui_magnification = 10;

			g_now.output(vec_result, 1, Palette::Aqua);
			g_now.outputInners(answer.main_frame);
			font(L"answer pattern : ", answer_count).draw(font_left_pos_x, font_left_pos_y + font_interval * 1);

			continue;
		}

		////////////////////配置情報利用機能//////////////////////
		//もし、かぶったときに面積が>0ならはじく。
		//配置情報のやつがかぶったとき面積==0なら組み付ける。
		//配置上のやつの判定関数と、配置情報専用の組付け関数
		//配置情報のやつは組み付けてなかったら色を塗らない

		//どのペースで組み合わせする？
		//各探索のところでいいかな

		//探索止めてから使ってね
		if (Input::KeyF3.clicked && !f.valid()) {
			//画像を取り込む
			int info_num = Puzzle::gui().takeInformationPicture();
			Window::Resize(1900, 1000);		

			std::vector<Puzzle::piece> arrange_info;

			for (int i = 1; i <= info_num; i++) {
				TextReader tr(L"cameraDataInfo" + ToString(i) + L".txt", TextEncoding::ANSI);
				
				tr.close();
				std::vector<Puzzle::piece> temp = Puzzle::file_manager::input_piece("cameraDataInfo" + std::to_string(i) + ".txt");
				copy(temp.begin(), temp.end(), std::back_inserter(arrange_info));
			}

			ClearPrint();

			//初期のステートにぶち込む
			//まず、比較判定をする。フラグ立てる。

			int j = 0;
			while (true) {

				for (int i = 0; i < p.size(); i++) {
					//ポリゴンの同一判定処理
					if (Puzzle::pieceEquals(p[i].piece_number, arrange_info[j].polygon)) {
						p[i].piece_arrange = true;

						//piece_managerの情報を書き換える
						//Puzzle::piece_manager::get_instance().getPolygon(p[i].piece_number) = arrange_info[j].polygon;
						Puzzle::piece_manager::get_instance().changePolygon(p[i].piece_number , arrange_info[j].polygon);
						p[i].polygon = arrange_info[j].polygon;
						j++;
					}
					if (j == arrange_info.size())break;
				}
				if (j == arrange_info.size())break;
			}

			//組み付けて、消去する。

			while (true) {
				bool f = true;
				for (int i = 0; i < p.size(); i++) {
					Puzzle::frame result;
					//ここですでに重なることは考慮しない
					if(p[i].piece_arrange)
					if (Puzzle::unionByArrange(F, p[i], result) == 0) {
						F = result;
						p.erase(p.begin() + i);
						i--;
						f = false;
					}
				}
				if (f)
					break;
			}
			//組付けられなかったらそのまま。

			//それをtargetStateにぶち込む

			targetState.clear();
			beforeState.clear();
			beforeState.push_back(Puzzle::state(F,p));
			nowState.clear();
			nowState.push_back(Puzzle::state(F,p));

		}

		//時間表示
		font(gameTimer.min(), L"min, ", gameTimer.s() % 60, L"s").draw(font_left_pos_x, font_left_pos_y + font_interval * 1);

		//探索の行き止まりに行ったら
		if (answer_count == -1) {
			font(L"Not Exists Next State.").draw(font_left_pos_x, font_left_pos_y + font_interval * 2);
		}

		////////////////beforeState用の表示////////////////

		if (beforeState.size() != 0) {

			g_now.outputInners(beforeState[(view_page) % beforeState.size()].main_frame, 3);
			auto & composition = beforeState[(view_page) % beforeState.size()].main_frame.getComposition();
			for (int i = 0; i < composition.size(); i++) {
				g_now.outputFill(composition[i], HSV(i * 25).toColorF(0.5));
			}
			g_now.output(beforeState[(view_page) % beforeState.size()].main_frame.getComposition(), 1, Palette::Aqua);
			g_now.outputWithArea(beforeState[(view_page) % beforeState.size()].main_frame.getComposition());

			int viewer_piece_pos_x = 8 * 100 + 50 + 4 * 100 + 50;
			int viewer_piece_pos_y = 10;
			for (auto& i : beforeState[(view_page) % beforeState.size()].pieces) {

				if (i.piece_arrange == true) {
					g_now.output(Puzzle::piece_manager::get_instance().getPolygon(i.piece_num),3,Palette::Gray);
				}
				else {
					viewer_piece.gui_move_x = viewer_piece_pos_x - Puzzle::piece_manager::get_instance().getPolygon(i.piece_num).outer()[0].x();
					viewer_piece.gui_move_y = viewer_piece_pos_y - Puzzle::piece_manager::get_instance().getPolygon(i.piece_num).outer()[0].y();
					viewer_piece_pos_x = viewer_piece.gui_move_x + 100;
					if (viewer_piece_pos_x >= 8 * 100 + 50 + 4 * 100 + 50 + 400) {
						viewer_piece_pos_y += 65;
						viewer_piece_pos_x = 8 * 100 + 50 + 4 * 100 + 50;
					}

					viewer_piece.output(Puzzle::piece_manager::get_instance().getPolygon(i.piece_num), 2, Palette::Yellow);
				}
			}


			font(L"comp count : ", beforeState[(view_page) % beforeState.size()].main_frame.getComposition().size()).draw(font_left_pos_x, font_left_pos_y + font_interval * 3);
			font(L"piece count : ", beforeState[(view_page) % beforeState.size()].pieces.size()).draw(font_left_pos_x, font_left_pos_y + font_interval * 4);
			font(L"view piece num: ", (view_page) % beforeState.size(), L" score: ", beforeState[(view_page) % beforeState.size()].score
				//, L" piece: ", beforeState[(view_page) % beforeState.size()].main_frame.pre_perimeter()
				, L" possible: ", beforeState[(view_page) % beforeState.size()].isAnswerPossibility()
				, L" piece: ", beforeState[(view_page) % beforeState.size()].minPieceEdge()
				, L" innner: ", beforeState[(view_page) % beforeState.size()].minInnerEdge()
			).draw(font_left_pos_x, font_left_pos_y + font_interval * 5);
			//Running追加
			font(L"before queue size : ", beforeState.size()).draw(font_left_pos_x, font_left_pos_y + font_interval * 6);
			font(L"now queue size : ", nowStateSize).draw(font_left_pos_x, font_left_pos_y + font_interval * 7);

			//beforeStateのViewer操作
			if (gui.button(L"next_view").pushed) {
				view_page++;
				if (view_page >= (int)beforeState.size())
					view_page = 0;
			}
			if (gui.button(L"prev_view").pushed) {
				view_page--;
				if (view_page < 0)
					view_page = (int)beforeState.size() - 1;
			}

			if (gui.toggleSwitch(L"auto").isRight) {
				if (sw.isPaused()) {
					sw.start();
				}
				if (sw.elapsed() > 100ms) {
					sw.restart();
					view_page++;
					if (view_page >= (int)beforeState.size())
						view_page = 0;
				}
			}

			//ターゲット、現在の探索が終了したとき選んだターゲットを起点としてまた１から探索が始まる。
			if (gui.button(L"add").pushed) {
				targetState.push_back(beforeState[(view_page) % beforeState.size()]);
				historyState.push_back(beforeState[(view_page) % beforeState.size()]);
			}

			//辺、頂点を押したらそこにピースをくっつけたstateを列挙する。処理。
			//スレッド分けるまでも無いかな。

			//まずは頂点
			//距離が５以下の頂点を光らせる。
			//クリックしてたら返す。
			//くそだけど早い使いやすい。便利が良い。

			std::pair<int, int> select_point = g_now.selectPoint(beforeState[(view_page) % beforeState.size()].main_frame);

			if (select_point != std::make_pair(-1, -1)) {

				//うーん、後で変えるべき
				auto& temp = beforeState[(view_page) % beforeState.size()].getNextStateSelectPoint(select_point.first, select_point.second);
				for (auto & i : temp) {
					i.score = (beforeState[(view_page) % beforeState.size()].main_frame.perimeter()
						+ i.main_frame.pre_perimeter()
						- i.main_frame.perimeter()) / 2;
				}


				temp.push_back(beforeState[(view_page) % beforeState.size()]);

				sort(temp.begin(), temp.end(), [](const Puzzle::state& a, const Puzzle::state& b) {
					return a.score > b.score;
				});

				beforeState = temp;

				if (!f.valid()) {

					view_page = 0;
					sw.restart();
					progress = 0;
					shareProgress = 0;

					nowState = beforeState;
					nowStateSize = nowState.size();

				}
			}


			//辺のバージョン
			//距離が５以下の辺を光らせる。
			//クリックしてたら返す。
			//くそだけど早い使いやすい。便利が良い。

			select_point = g_now.selectEdge(beforeState[(view_page) % beforeState.size()].main_frame);

			if (select_point != std::make_pair(-1, -1)) {

				//うーん、後で変えるべき
				auto& temp = beforeState[(view_page) % beforeState.size()].getNextStateSelectEdge(select_point.first, select_point.second);
				for (auto & i : temp) {
					i.score = (beforeState[(view_page) % beforeState.size()].main_frame.perimeter()
						+ i.main_frame.pre_perimeter()
						- i.main_frame.perimeter()) / 2;
				}


				temp.push_back(beforeState[(view_page) % beforeState.size()]);

				sort(temp.begin(), temp.end(), [](const Puzzle::state& a, const Puzzle::state& b) {
					return a.score > b.score;
				});

				beforeState = temp;

				if (!f.valid()) {

					view_page = 0;
					sw.restart();
					progress = 0;
					shareProgress = 0;

					nowState = beforeState;
					nowStateSize = nowState.size();

				}
			}

			if (gui.button(L"bef_erase").pushed) {
				if (beforeState.size() != 1) {
					beforeState.erase(beforeState.begin() + (view_page) % beforeState.size());
					if (view_page == beforeState.size()) {
						view_page == 0;
					}
				}
			}



		}

		///////////////history用の表示////////////////////

		if (historyState.size() != 0) {

			//ヒストリーの表示
			history.outputInners(historyState[(history_view_page) % historyState.size()].main_frame, 3);
			auto & composition = historyState[(history_view_page) % historyState.size()].main_frame.getComposition();
			for (int i = 0; i < composition.size(); i++) {
				history.outputFill(composition[i], HSV(i * 25).toColorF(0.5));
			}
			history.output(historyState[(history_view_page) % historyState.size()].main_frame.getComposition(), 1, Palette::Aqua);



			//次に送る。
			if (gui.button(L"his_next_view").pushed) {
				history_view_page++;
				if (history_view_page >= (int)historyState.size())
					history_view_page = 0;
			}

			//前に戻す。
			if (gui.button(L"his_prev_view").pushed) {
				history_view_page--;
				if (history_view_page < 0)
					history_view_page = (int)historyState.size() - 1;
			}

			//今見ている状態をtargetStateにもって行く。
			if (gui.button(L"his_copy").pushed) {
				targetState.clear();
				targetState.push_back(historyState[(history_view_page) % historyState.size()]);
			}
		}

		////////////////targetState用の表示////////////////

		if (targetState.size() != 0) {

			//除去機能ここに実装
			//難しいので延期。もしかして実装しないかも

			//targetState
			//if clicked


			g_target.outputInners(targetState[(target_view_page) % targetState.size()].main_frame, 3);
			auto & composition = targetState[(target_view_page) % targetState.size()].main_frame.getComposition();
			for (int i = 0; i < composition.size(); i++) {
				g_target.outputFill(composition[i], HSV(i * 25).toColorF(0.5));
			}
			g_target.output(targetState[(target_view_page) % targetState.size()].main_frame.getComposition(), 1, Palette::Aqua);

			font(L"target queue size : ", targetState.size()).draw(font_left_pos_x, font_left_pos_y + font_interval * 8);
			font(L"target view piece num : ", target_view_page % targetState.size(), L"  score : ", targetState[(target_view_page) % targetState.size()].score).draw(font_left_pos_x, font_left_pos_y + font_interval * 9);
			font(L"target piece count : ", targetState[(target_view_page) % targetState.size()].pieces.size()).draw(font_left_pos_x, font_left_pos_y + font_interval * 10);

			//GUI操作

			////////Viewer操作/////////////

			if (gui.button(L"tag_next_view").pushed) {
				target_view_page++;
				if (target_view_page >= (int)targetState.size())
					target_view_page = 0;
			}
			if (gui.button(L"tag_prev_view").pushed) {
				target_view_page--;
				if (target_view_page < 0)
					target_view_page = (int)targetState.size() - 1;
			}

			if (gui.toggleSwitch(L"tag_auto").isRight) {
				if (tag_sw.isPaused()) {
					tag_sw.start();
				}
				if (tag_sw.elapsed() > 100ms) {
					tag_sw.restart();
					target_view_page++;
					if (target_view_page >= (int)targetState.size())
						target_view_page = 0;
				}
			}

			//targetStateが１つ以上ある場合のみ使える処理。
			//探索を終了させ、現在のtargetStateの中の状態を使って再探索する。
			if (gui.button(L"end").pushed) {
				target_flag = true;
				answer_count = 0;

				if (f.valid()) {
					mtx.lock();
					shareProgress = -1;
					mtx.unlock();
				}
				else {
					view_page = 0;
					sw.restart();
					progress = 0;
					shareProgress = 0;


					beforeState = nowState = targetState;
					target_flag = false;
					nowStateSize = nowState.size();
					targetState.clear();

					nextState.clear();
				}

			}

			if (gui.button(L"clear").pushed) {
				targetState.clear();
				target_view_page = 0;
			}


		}

		if (swDelay.ms() < 1000 || answer_count == -1) {
			//if(!Input::KeyS.clicked){
		}
		else {
			if (f.valid()) {
				auto status = f.wait_for(std::chrono::milliseconds(0));
				bool isGet = (status == std::future_status::ready);

				if (isGet) {
					count++;
					answer_count = f.get();

					view_page = 0;
					sw.restart();
					progress = 0;
					shareProgress = 0;

					if (target_flag) {
						nowState = targetState;
						target_flag = false;
						nowStateSize = nowState.size();
						targetState.clear();
					}
					else {
						nowState = nextState;
						nowStateSize = nowState.size();
					}

					Puzzle::file_manager::output_error(std::to_string(count) + ":" + std::to_string(nowState.size()));

					nextState.clear();

					if (nowStateSize == 0) {
						answer_count = -1;
						continue;
					}
					else {
						beforeState = nowState;
					}


				}
				else {
					mtx.lock();
					progress = shareProgress;
					mtx.unlock();
				}

			}

			//サーチ関係の値を管理するGUIのみ分離。
			if (!f.valid() && gui.toggleSwitch(L"advance_state").isRight) {

				int funcEnum = *guiSearch.radioButton(L"func_policy").checkedItem, funcEval = *guiSearch.radioButton(L"eval_policy").checkedItem;

				//探索方針を８まで準備可能
				funcEnum += guiSearch.toggleSwitch(L"small_policy").isRight * 8;//(gui.radioButton(L"func_policy").num_items)

				//チョクダイサーチ
				if (*guiSearch.radioButton(L"search_policy").checkedItem == 0) {
					f = std::async(std::launch::async, (Puzzle::beam_search::solveGuiSuzukiFunc),
						std::ref(nowState), std::ref(nextState),
						funcEnum, funcEval,
						std::ref(answer), std::ref(mtx), std::ref(shareProgress));
				}
				//ビームサーチ
				else if (*guiSearch.radioButton(L"search_policy").checkedItem == 1) {
					f = std::async(std::launch::async, (Puzzle::beam_search::chokudaiSearchBySuzuki),
						std::ref(nowState), std::ref(nextState),
						funcEnum, funcEval,
						std::ref(answer), std::ref(mtx), std::ref(shareProgress));
				}
				//全探索
				else if (*guiSearch.radioButton(L"search_policy").checkedItem == 2) {
					f = std::async(std::launch::async, (Puzzle::beam_search::fullSearchBySuzuki),
						std::ref(nowState), std::ref(nextState),
						funcEnum, funcEval,
						std::ref(answer), std::ref(mtx), std::ref(shareProgress));
				}
				//Beam Stack Search
				else if (*guiSearch.radioButton(L"search_policy").checkedItem == 3) {
					f = std::async(std::launch::async, (Puzzle::beam_search::beamStackSearchBySuzuki),
						std::ref(nowState), std::ref(nextState),
						funcEnum, funcEval,
						std::ref(answer), std::ref(mtx), std::ref(shareProgress));
				}
				progress = 101;
			}

			swDelay.restart();
		}

	}

	if (f.valid()) {
		mtx.lock();
		shareProgress = -1;
		mtx.unlock();

		answer_count = f.get();
	}

	Puzzle::file_manager::output_error("正常に終了しました。");
}
